SRC_DIR := schess
OBJ_DIR := obj
TARGET_DIR := target
TEST_SRC_DIR := test
TEST_OBJ_DIR := $(OBJ_DIR)/test
DEPTH := 7
FEN := FENs/init.fen
ARGS := $(FEN) $(DEPTH)

SRC := $(wildcard $(SRC_DIR)/*.c)
OBJ := $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SRC))
BIN := $(TARGET_DIR)/schess
TEST_SRC := $(wildcard $(TEST_SRC_DIR)/*.c)
TEST_OBJ := $(patsubst $(TEST_SRC_DIR)/%.c, $(TEST_OBJ_DIR)/%.o, $(TEST_SRC))
TEST_BIN := $(TARGET_DIR)/schess_tests

CFLAGS := -Wall -Wextra -pedantic -O3 -I.
CFLAGS += -std=c2x
CFLAGS += -mbmi2

.PHONY: all debug clean run test

all: $(BIN)

run: all
	$(BIN) $(ARGS)

test: $(TEST_BIN)
	$(TEST_BIN)

$(TEST_BIN): $(TEST_OBJ) $(OBJ) | $(TARGET_DIR)
	$(CC) $(LDFLAGS) $(filter-out $(OBJ_DIR)/schess.o, $^) $(LDLIBS) -o $@

$(TEST_OBJ_DIR)/%.o: $(TEST_SRC_DIR)/%.c | $(TEST_OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

debug: CFLAGS := $(filter-out -O3, $(CFLAGS))
debug: CFLAGS += -ggdb
debug: $(BIN) $(TEST_BIN)

$(BIN): $(OBJ) | $(TARGET_DIR)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET_DIR) $(OBJ_DIR) $(TEST_OBJ_DIR):
	mkdir -p $@

clean:
	@$(RM) -rv $(TARGET_DIR) $(OBJ_DIR)

-include $(OBJ:.o=.d)
