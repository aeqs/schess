#!/bin/sh

CONFIG_HEADER="ntn.h"

config=$(cat $1 | awk 'NF' | sed 's/,//g')

tuple_count=$(echo "$config" | wc -l)
tuple_sizes=$(echo "$config" | awk '{print NF}' | tr '\n' ',' | sed 's/^/{/;s/,$/}/')
nodes=$(echo "$config" | awk '{$1=$1; print}' | sed 's/ /,/g;s/^/(square[]){/;s/$/},/;1s/^/{\n/;$a\}')

result=$(
echo "#ifndef CONFIG_NTN_H";
echo "#define CONFIG_NTN_H";
echo "#include <schess/ntn.h>";
echo "#include <schess/types.h>";
echo "ntup_net *config_ntn_create(void)";
echo "{";
echo "return ntn_create((size_t) $tuple_count, (size_t [$tuple_count]) $tuple_sizes, (square *[$tuple_count])$nodes);";
echo "}";
echo "#endif // CONFIG_NTN_H";
)

echo "$result" > $CONFIG_HEADER

