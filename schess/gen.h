#ifndef SCHESS_GEN_H
#define SCHESS_GEN_H

#include <schess/types.h>
#include <stddef.h>

#define MAX_MOVES_NUM 256
typedef struct
{
  size_t size, sorted;
  move_t moves[MAX_MOVES_NUM];
} move_buffer;

move_t *move_buffer_next(move_buffer *mbuf);

size_t generate_moves(game_state *game, move_buffer *out);

int king_safety(board_state *board, color active);

#endif // SCHESS_GEN_H
