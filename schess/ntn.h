#ifndef SCHESS_NTN_H
#define SCHESS_NTN_H

#include <schess/types.h>
#include <stddef.h>

typedef struct ntup_net ntup_net;

ntup_net *ntn_create(size_t tuple_count, size_t tuple_sizes[tuple_count], square *nodes[tuple_count]);
void      ntn_destroy(ntup_net *ntn);
void      ntn_update(ntup_net *ntn, board_state *board, int score, float learning_rate);
int       ntn_evaluate(ntup_net *ntn, board_state *board);
void      ntn_save(ntup_net *ntn, const char *fname);
ntup_net *ntn_load(const char *fname);

#endif // SCHESS_NTN_H
