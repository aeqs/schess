#ifndef SCHESS_TIME_H
#define SCHESS_TIME_H

#include <time.h>

typedef struct stop_cond stop_cond_t;

stop_cond_t *sc_create(void);
void         sc_destroy(stop_cond_t *sc);

int  sc_timeout(stop_cond_t *sc, struct timespec timeout);
int  sc_test(stop_cond_t *sc);
void sc_stop(stop_cond_t *sc);

#endif // SCHESS_TIME_H
