#include <schess/time.h>
#include <schess/utils.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <threads.h>
#include <time.h>

enum sc_status
{
  SC_INFT, // no timeout set
  SC_WAIT, // still waiting
  SC_STOP, // timeout occured
  SC_DONE, // stopped and waiter thread joined
};

struct stop_cond
{
  atomic_int status;
  struct timespec timeout;

  thrd_t tid;
  mtx_t mutex_cond, mutex_control;
  cnd_t condition;
};

stop_cond_t *
sc_create(void)
{
  stop_cond_t *sc;
  sc = malloc(sizeof(stop_cond_t));

  if (sc == NULL)
    return NULL;

  sc->status = SC_INFT;

  if (mtx_init(&sc->mutex_cond, mtx_plain))
  {
    free(sc);
    return NULL;
  }
  if (mtx_init(&sc->mutex_control, mtx_plain))
  {
    mtx_destroy(&sc->mutex_cond);
    free(sc);
    return NULL;
  }
  if (cnd_init(&sc->condition))
  {
    mtx_destroy(&sc->mutex_control);
    mtx_destroy(&sc->mutex_cond);
    free(sc);
    return NULL;
  }

  return sc;
}

void
sc_destroy(stop_cond_t *sc)
{
  sc_stop(sc);

  cnd_destroy(&sc->condition);
  mtx_destroy(&sc->mutex_cond);
  mtx_destroy(&sc->mutex_control);
  free(sc);
}

static void
sc_stop_unlocked(stop_cond_t *sc)
{
  enum sc_status status;

  mtx_lock(&sc->mutex_cond) ASSERT_RET(thrd_success, 1, "sc_stop_unlocked(): mtx_lock()");
  cnd_signal(&sc->condition) ASSERT_RET(thrd_success, 1, "sc_stop_unlocked(): cnd_signal()");
  mtx_unlock(&sc->mutex_cond) ASSERT_RET(thrd_success, 1, "sc_stop_unlocked(): mtx_unlock()");

  status = atomic_exchange(&sc->status, SC_DONE);
  // collect thread if timer running or ran out
  if (status == SC_WAIT || status == SC_STOP)
    thrd_join(sc->tid, NULL) ASSERT_RET(thrd_success, 1, "sc_stop_unlocked(): thrd_join()");
}

void
sc_stop(stop_cond_t *sc)
{
  mtx_lock(&sc->mutex_control) ASSERT_RET(thrd_success, 1, "sc_stop(): mtx_lock()");
  sc_stop_unlocked(sc);
  mtx_unlock(&sc->mutex_control) ASSERT_RET(thrd_success, 1, "sc_stop(): mtx_unlock()");
}

static int
sc_timeout_handler(void *arg)
{
  stop_cond_t *sc;
  struct timespec end_time;
  int err;

  sc = (stop_cond_t *) arg;

  timespec_get(&end_time, TIME_UTC) ASSERT_RET(TIME_UTC, 1, "sc_timeout_handler(): timespec_get()");

  end_time.tv_sec += sc->timeout.tv_sec;
  end_time.tv_nsec += sc->timeout.tv_nsec;
  if (end_time.tv_nsec >= 1000000000) {
    end_time.tv_sec += 1;
    end_time.tv_nsec -= 1000000000;
  }

  mtx_lock(&sc->mutex_cond) ASSERT_RET(thrd_success, 1, "sc_timeout_handler(): mtx_lock()");
  do
  {
    err = cnd_timedwait(&sc->condition, &sc->mutex_cond, &end_time);
  } while (err == thrd_success && atomic_load(&sc->status) == SC_WAIT);
  mtx_unlock(&sc->mutex_cond) ASSERT_RET(thrd_success, 1, "sc_timeout_handler(): mtx_unlock()");

  if (err == thrd_timedout)
    atomic_store(&sc->status, SC_STOP);
  else
    err ASSERT_RET(thrd_success, 1, "sc_timeout_handler(): cnd_timedwait()");
  thrd_exit(thrd_success);
}

int
sc_timeout(stop_cond_t *sc, struct timespec timeout)
{
  int err;

  mtx_lock(&sc->mutex_control) ASSERT_RET(thrd_success, 1, "sc_timeout(): mtx_lock()");
  sc_stop_unlocked(sc);

  if (timeout.tv_nsec == 0 && timeout.tv_sec == 0)
  {
    atomic_store(&sc->status, SC_INFT);
    mtx_unlock(&sc->mutex_control) ASSERT_RET(thrd_success, 1, "sc_timeout(): mtx_unlock()");
    return 0;
  }

  sc->timeout = timeout;
  err = thrd_create(&sc->tid, sc_timeout_handler, (void *) sc);
  if (err == thrd_success)
    atomic_store(&sc->status, SC_WAIT);
  mtx_unlock(&sc->mutex_control) ASSERT_RET(thrd_success, 1, "sc_timeout(): mtx_unlock()");

  return err;
}

int
sc_test(stop_cond_t *sc)
{
  enum sc_status status;

  status = atomic_load(&sc->status);

  return
    status == SC_STOP ||
    status == SC_DONE;
}
