#ifndef SCHESS_LUT_H
#define SCHESS_LUT_H

#include <schess/types.h>

#define LUT_BISHOP_SIZE 5248
#define LUT_ROOK_SIZE 102400

extern bitboard lut_bishop_mask    [NUM_SQUARES];
extern bitboard lut_bishop_offset  [NUM_SQUARES];
extern bitboard lut_bishop_attacks [LUT_BISHOP_SIZE];

extern bitboard lut_rook_mask      [NUM_SQUARES];
extern bitboard lut_rook_offset    [NUM_SQUARES];
extern bitboard lut_rook_attacks   [LUT_ROOK_SIZE];

extern bitboard lut_king_attacks   [NUM_SQUARES];
extern bitboard lut_knight_attacks [NUM_SQUARES];

void lut_init(void);

#endif // SCHESS_LUT_H
