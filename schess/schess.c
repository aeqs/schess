#include <errno.h>
#include <schess/gen.h>
#include <schess/lut.h>
#include <schess/move.h>
#include <schess/ntn.h>
#include <schess/search.h>
#include <schess/schess.h>
#include <schess/time.h>
#include <schess/tt.h>
#include <schess/types.h>
#include <schess/uci.h>
#include <schess/utils.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TT_CAPACITY (1 << 25)
#define TT_BUCKET_SIZE (3)

#define UCI_CMD_MAX (1 << 14)

static void
uci_loop(void)
{
  char line_buf[UCI_CMD_MAX];
  int err;

  while (fgets(line_buf, UCI_CMD_MAX, stdin) != NULL)
  {
    if (*str_next_nonspace(line_buf) == '\0')
      continue;

    err = uci_handle_command(line_buf);
    if (err)
    {
      fprintf(stderr, "Invalid UCI command: %s", line_buf);
    }
    fflush(stdout);
  }
}

int
main(int argc, char **argv)
{
  /*
  if (argc == 1) return EXIT_SUCCESS;
  if (argc >  3) return EXIT_FAILURE;
  */

  schess_init();

  if (1 || (argc == 2 && strcmp(argv[1], "uci") == 0))
  {
    uci_init() ASSERT_RET(0, 1, "main(): uci_init()");
    uci_loop();
    return 0;
  }

  printf("SCHESS ENGINE by Kilian Chung\n");

  FILE *fp = fopen(argv[1], "rb");
  size_t length;
  char *FEN = 0;
  game_state game;

  if (!fp)
  {
    fprintf(stderr, "Error opening %s: %s\n", argv[1], strerror(errno));
    return EXIT_FAILURE;
  }

  fseek(fp, 0, SEEK_END);
  length = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  FEN = malloc(length);
  if (!FEN)
  {
    fprintf(stderr, "Error allocating string buffer: %s\n", strerror(errno));
    return EXIT_FAILURE;
  }
  fread(FEN, 1, length, fp);
  fclose (fp);

  unsigned depth = strtoul(argv[2], NULL, 10);

  trans_table_t *tt, *qstt;
  stop_cond_t *sc;
  struct search_result res;
  struct search_limits limits = search_limits_infinite;
  clock_t t;
  limits.depth = depth;

  sc = sc_create();
  tt = tt_create(TT_CAPACITY, TT_BUCKET_SIZE);
  qstt = tt_create(TT_CAPACITY, TT_BUCKET_SIZE);

  struct search_state sstate = { .qstt = qstt, .tt = tt, .sc = sc };

  parse_FEN(FEN, &game, NULL) ASSERT_RET(0, 1, NULL);
  printf("Board read in:\n");
  print_board(&game.dynamics.board);
  t = clock();
  res = search_root(&game, sstate, limits);
  t = clock() - t;
  printf("Best Move found: ");
  print_move(&game.dynamics.board, res.move);
  printf("\nWith Score: %d\nAt depth: %lu\n", res.score, res.depth);

  double time_taken = ((double) t) / CLOCKS_PER_SEC;
  printf("%.2lf kN/S\n", (((double) res.nodes) / time_taken) / 1000);

  tt_destroy(tt);
  tt_destroy(qstt);
  sc_destroy(sc);

  return EXIT_SUCCESS;
}
