#ifndef SCHESS_MOVES_H
#define SCHESS_MOVES_H

#include <schess/types.h>

void zobrist_random_init(int seed);

int move_make(move_t *m, game_state *game);
void move_unmake(move_t *m, game_state *game);
int move_is_null(move_t m);


int post_move_king_safety(move_t *m, game_state *game);

#endif // SCHESS_MOVES_H
