#ifndef SCHESS_TT_H
#define SCHESS_TT_H

#include <schess/types.h>
#include <stddef.h>

enum tt_entry_type
{
  TT_EMPTY,
  TT_EXACT,
  TT_ALPHA,
  TT_BETA,
};

struct tt_entry
{
  hash_t zhash;
  move_t best;
  unsigned born, depth;
  int score;
  enum tt_entry_type type;
};

typedef struct trans_table trans_table_t;

struct tt_entry *tt_probe(trans_table_t *tt, hash_t zhash);
void             tt_store(trans_table_t *tt, struct tt_entry entry);

trans_table_t *tt_create(size_t capacity, size_t bucket_size);
void           tt_destroy(trans_table_t *tt);

#endif // SCHESS_TT_H
