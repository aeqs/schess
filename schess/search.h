#ifndef SCHESS_SEARCH_H
#define SCHESS_SEARCH_H

#include <schess/ntn.h>
#include <schess/time.h>
#include <schess/tt.h>
#include <schess/types.h>

struct search_limits
{
  unsigned long depth, mate, nodes, time;
};
extern const struct search_limits search_limits_infinite;

struct search_state
{
  stop_cond_t *sc;
  trans_table_t *tt, *qstt;
  ntup_net *ntn;
};

struct search_result
{
  move_t move;
  unsigned long depth, nodes;
  int score;
} search_root(game_state *game, struct search_state sstate, struct search_limits limits);

#endif // SCHESS_SEARCH_H
