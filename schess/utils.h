#ifndef SCHESS_UTILS_H
#define SCHESS_UTILS_H

#include <schess/gen.h>
#include <schess/tt.h>
#include <schess/types.h>

#define BITMASK(bits) (bits >= sizeof(unsigned long long) * 8 ? ((unsigned long long) 0) - 1 : ((unsigned long long) 1 << (bits)) - 1)

unsigned str_skip_blanks(const char **str);
const char *str_next_nonspace(const char *str);
int str_match(const char **str, const char *cmp);

void error(int status, const char *msg);
#define ASSERT_RET(ret_, status_, msg_) == (ret_) ? (void) 0 : error((status_), (msg_))
#define ASSERT_NOT(ret_, status_, msg_) == (ret_) ? error((status_), (msg_)) : (void) 0

const char *move_name(enum MOVE_TYPE mt);
const char *piece_name(piece_type pt);
const char *square_name(square sq);
square square_from_name(char file, char rank);

int parse_FEN(const char *FEN, game_state *game, const char **end_ptr);

void print_board(board_state *board);
void print_moves(board_state *board, move_buffer *mbuf);
void print_move(board_state *board, move_t m);

int parse_LAN(const char *LAN, game_state *game, move_t *move_out, const char **end_ptr);
int parse_SAN(const char *SAN, game_state *game, move_t *move_out, const char **end_ptr);

void print_pv(game_state *game, trans_table_t *tt);

#endif // SCHESS_UTILS_H
