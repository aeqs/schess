#include <ctype.h>
#include <limits.h>
#include <schess/move.h>
#include <schess/search.h>
#include <schess/time.h>
#include <schess/tt.h>
#include <schess/types.h>
#include <schess/uci.h>
#include <schess/utils.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>

static struct
{
  struct search_state sstate;
  struct search_limits limits;
  thrd_t search_tid;
  int searching;
  game_state game;
  const char *cmd;
} UCI;

#define TT_DEFAULT_CAPACITY (1 << 25)
#define TT_DEFAULT_BUCKET_SIZE (3)

static int
uci_cmd_done(void)
{
  return *UCI.cmd == '\0' || *UCI.cmd == '\n';
}

int
uci_init(void)
{
  UCI.sstate.sc = sc_create();
  if (UCI.sstate.sc == NULL)
    return 1;

  UCI.sstate.tt   = tt_create(TT_DEFAULT_CAPACITY, TT_DEFAULT_BUCKET_SIZE);
  if (UCI.sstate.tt == NULL)
  {
    sc_destroy(UCI.sstate.sc);
    return 1;
  }
  UCI.sstate.qstt = tt_create(TT_DEFAULT_CAPACITY, TT_DEFAULT_BUCKET_SIZE);
  if (UCI.sstate.qstt == NULL)
  {
    tt_destroy(UCI.sstate.tt);
    sc_destroy(UCI.sstate.sc);
    return 1;
  }

  UCI.searching = 0;

  if (uci_handle_command("position startpos"))
  {
    tt_destroy(UCI.sstate.qstt);
    tt_destroy(UCI.sstate.tt);
    sc_destroy(UCI.sstate.sc);
    return 1;
  }

  return 0;
}

static int
id(void)
{
  printf("id name schess\n");
  printf("id author Kilian Chung\n");
  return 0;
}

static int
uci(void)
{
  int err;

  err = id();
  if (err)
    return err;

  printf("uciok\n");
  return 0;
}

static int
debug(void)
{
  return 0;
}

static int
isready(void)
{
  printf("readyok\n");
  return 0;
}

static int
setoption(void)
{
  return 0;
}

static const char *startpos = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

// Return On Error
#define ROE(_expr) do \
{ \
  int _err = (_expr); \
  if (_err) \
    return _err; \
} while (0)
// Make Sure That
#define MST(_expr) do \
{ \
  int _res = (_expr); \
  if (!_res) \
    return 1; \
} while (0)

static int
position(void)
{
  game_state game;
  move_t m;

  if (str_match(&UCI.cmd, "startpos"))
    ROE(parse_FEN(startpos, &game, NULL));
  else if (str_match(&UCI.cmd, "fen"))
  {
    MST(str_skip_blanks(&UCI.cmd));
    ROE(parse_FEN(UCI.cmd, &game, &UCI.cmd));
  }
  else return 1;

  MST(uci_cmd_done() || str_skip_blanks(&UCI.cmd));

  if (uci_cmd_done())
  {
    UCI.game = game;
    return 0;
  }

  MST(str_match(&UCI.cmd, "moves"));

  while (1)
  {
    MST(uci_cmd_done() || str_skip_blanks(&UCI.cmd));

    if (uci_cmd_done())
      break;

    ROE(parse_LAN(UCI.cmd, &game, &m, &UCI.cmd));

    move_make(&m, &game);
  }

  UCI.game = game;
  return 0;
}

#define TT_CAPACITY (1 << 25)
#define TT_BUCKET_SIZE (3)

#define MAX_PLY 256

static int
start_search(void *arg)
{
  (void) arg;

  struct search_result res;
  game_state game = UCI.game;

  res = search_root(&game, UCI.sstate, UCI.limits);

  if (move_is_null(res.move))
    printf("bestmove 0000\n");
  else
    printf("bestmove %s%s\n", square_name(res.move.from), square_name(res.move.to));
  fflush(stdout);
  return 0;
}

static int stop(void);

static int
go(void)
{
  memcpy((void *) &UCI.limits, (const void *) &search_limits_infinite, sizeof(struct search_limits));

  while (!uci_cmd_done())
  {

    if (0);

#define UCI_GO_STOP_CRITERION_CASE(_str, _loc) \
    else if (str_match(&UCI.cmd, (_str))) \
    { \
      MST(str_skip_blanks(&UCI.cmd) && isdigit(*UCI.cmd)); \
      (_loc) = strtoul(UCI.cmd, (char **) &UCI.cmd, 10); \
      MST((_loc) != ULONG_MAX); \
    }

    UCI_GO_STOP_CRITERION_CASE("depth"   , UCI.limits.depth)
    UCI_GO_STOP_CRITERION_CASE("nodes"   , UCI.limits.nodes)
    UCI_GO_STOP_CRITERION_CASE("mate"    , UCI.limits.mate )
    UCI_GO_STOP_CRITERION_CASE("movetime", UCI.limits.time )

#undef UCI_GO_STOP_CRITERION_CASE

    else if (str_match(&UCI.cmd, "infinite"))
      memcpy(&UCI.limits, &search_limits_infinite, sizeof(struct search_limits));

    else if (str_match(&UCI.cmd, "ponder"))
      ; // TODO: pondering

    // ignore time related commands
    else if (str_match(&UCI.cmd, "wtime"))      MST(str_skip_blanks(&UCI.cmd) && isdigit(*UCI.cmd) && strtoull(UCI.cmd, (char **) &UCI.cmd, 10) != ULLONG_MAX);
    else if (str_match(&UCI.cmd, "btime"))      MST(str_skip_blanks(&UCI.cmd) && isdigit(*UCI.cmd) && strtoull(UCI.cmd, (char **) &UCI.cmd, 10) != ULLONG_MAX);
    else if (str_match(&UCI.cmd, "winc" ))      MST(str_skip_blanks(&UCI.cmd) && isdigit(*UCI.cmd) && strtoull(UCI.cmd, (char **) &UCI.cmd, 10) != ULLONG_MAX);
    else if (str_match(&UCI.cmd, "binc" ))      MST(str_skip_blanks(&UCI.cmd) && isdigit(*UCI.cmd) && strtoull(UCI.cmd, (char **) &UCI.cmd, 10) != ULLONG_MAX);
    else if (str_match(&UCI.cmd, "movestogo" )) MST(str_skip_blanks(&UCI.cmd) && isdigit(*UCI.cmd) && strtoull(UCI.cmd, (char **) &UCI.cmd, 10) != ULLONG_MAX);

    MST(str_skip_blanks(&UCI.cmd) || uci_cmd_done());
  }

  stop();
  UCI.searching = 1;

  return thrd_create(&UCI.search_tid, start_search, NULL);
}

static int
stop(void)
{
  sc_stop(UCI.sstate.sc);

  if (UCI.searching)
    thrd_join(UCI.search_tid, NULL) ASSERT_RET(thrd_success, 1, "uci_handle_command(): stop(): thrd_join()");

  UCI.searching = 0;

  return 0;
}

static int
ponderhit(void)
{
  return 0;
}

static int
quit(void)
{
  exit(EXIT_SUCCESS);
}

static int
d(void)
{
  print_board(&UCI.game.dynamics.board);
  return 0;
}

int
uci_handle_command(const char *cmd)
{

#define UCI_ACTION_ON_CMD_AND_EXIT(action) do \
  { \
    if (str_match(&cmd, (#action))) \
    { \
      UCI.cmd = cmd; \
      if (!uci_cmd_done() && !str_skip_blanks(&UCI.cmd)) \
        return 1; \
      return (action)(); \
    } \
  } while (0)

  UCI_ACTION_ON_CMD_AND_EXIT(uci);
  UCI_ACTION_ON_CMD_AND_EXIT(debug);
  UCI_ACTION_ON_CMD_AND_EXIT(isready);
  UCI_ACTION_ON_CMD_AND_EXIT(setoption);
  UCI_ACTION_ON_CMD_AND_EXIT(position);
  UCI_ACTION_ON_CMD_AND_EXIT(go);
  UCI_ACTION_ON_CMD_AND_EXIT(stop);
  UCI_ACTION_ON_CMD_AND_EXIT(ponderhit);
  UCI_ACTION_ON_CMD_AND_EXIT(quit);

  UCI_ACTION_ON_CMD_AND_EXIT(d);

#undef UCI_ACTION_ON_CMD_AND_EXIT

  return 1;
}
