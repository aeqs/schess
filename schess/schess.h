#ifndef SCHESS_SCHESS_H
#define SCHESS_SCHESS_H

#include <schess/lut.h>
#include <schess/move.h>

static inline void
schess_init(void)
{
  zobrist_random_init(0);
  lut_init();
}

#endif // SCHESS_SCHESS_H
