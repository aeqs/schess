#ifndef SCHESS_EVAL_H
#define SCHESS_EVAL_H

#include <schess/types.h>

int eval_position(game_state *game);

#endif // SCHESS_EVAL_H
