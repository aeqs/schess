#include <immintrin.h>
#include <schess/gen.h>
#include <schess/lut.h>
#include <schess/types.h>
#include <stddef.h>
#include <stdlib.h>
#include <strings.h>

static inline bitboard
rook_attacks(bitboard occ, square sq)
{
  // X86
  return lut_rook_attacks[lut_rook_offset[sq] + _pext_u64(occ, lut_rook_mask[sq])];
}

static inline bitboard
bishop_attacks(bitboard occ, square sq)
{
  // X86
  return lut_bishop_attacks[lut_bishop_offset[sq] + _pext_u64(occ, lut_bishop_mask[sq])];
}

static inline bitboard
queen_attacks(bitboard occ, square sq)
{
  return rook_attacks(occ, sq) | bishop_attacks(occ, sq);
}

static inline square
log_bit(bitboard board)
{
  // TODO: danger; wrong word size
  // GCC
  return __builtin_ctzll(board);
}
static inline square
pop_bit(bitboard *board)
{
  square sq = log_bit(*board);
  *board ^= sq2bb(sq);
  return sq;
}


static inline void
move_buffer_append_move(square from, square to, piece_type capture, enum MOVE_TYPE type, move_buffer *out)
{
  out->moves[out->size] = (move_t)
  {
    .from      = from,
    .to        = to,
    .capture   = capture,
    .type      = type,
    .heuristic = 0,
  };

  ++out->size;
}

static inline void
move_buffer_append_attacks(bitboard attacks, square from, piece_type types[NUM_SQUARES], move_buffer *out)
{
  square to;

  while (attacks)
  {
    to = pop_bit(&attacks);

    move_buffer_append_move(from, to, types[to], MT_NORMAL, out);
  }
}

static inline void
generate_rook_moves(bitboard own, bitboard other, square sq, piece_type types[NUM_SQUARES], move_buffer *out)
{
  bitboard occ = own | other;
  bitboard attacks = rook_attacks(occ, sq) & ~own;
  move_buffer_append_attacks(attacks, sq, types, out);
}
static inline void
generate_bishop_moves(bitboard own, bitboard other, square sq, piece_type types[NUM_SQUARES], move_buffer *out)
{
  bitboard occ = own | other;
  bitboard attacks = bishop_attacks(occ, sq) & ~own;
  move_buffer_append_attacks(attacks, sq, types, out);
}
static inline void
generate_queen_moves(bitboard own, bitboard other, square sq, piece_type types[NUM_SQUARES], move_buffer *out)
{
  bitboard occ = own | other;
  bitboard attacks = queen_attacks(occ, sq) & ~own;
  move_buffer_append_attacks(attacks, sq, types, out);
}

static inline void
generate_knight_moves(bitboard own, bitboard other, square sq, piece_type types[NUM_SQUARES], move_buffer *out)
{
  (void) other;
  bitboard attacks = lut_knight_attacks[sq] & ~own;
  move_buffer_append_attacks(attacks, sq, types, out);
}

static inline int
is_square_checked(bitboard own, bitboard other, bitboard other_pieces[6], bitboard other_pawn_attacks, square sq)
{
  bitboard attacker;
  bitboard occ = own | other;

  attacker  = rook_attacks(occ, sq)   & (other_pieces[PR_R] | other_pieces[PR_Q]);
  attacker |= bishop_attacks(occ, sq) & (other_pieces[PR_B] | other_pieces[PR_Q]);
  attacker |= lut_knight_attacks[sq]  & (other_pieces[PR_N]);
  attacker |= other_pawn_attacks      & sq2bb(sq);

  // Optional for pseudo-legal, obligatory for legal checker
  attacker |= lut_king_attacks[sq]    & (other_pieces[PR_K]);

  return attacker != 0;
}


static inline void
generate_king_moves(bitboard own, bitboard other, bitboard other_pieces[6], bitboard other_pawn_attacks, square sq, piece_type types[NUM_SQUARES], irreversible_state meta, move_buffer *out)
{
  bitboard occ = own | other;
  bitboard attacks = lut_king_attacks[sq] & ~own;
  move_buffer_append_attacks(attacks, sq, types, out);

  // is king checked?
  if (is_square_checked(own, other, other_pieces, other_pawn_attacks, sq)) return;

  // castling moves
  bitboard castle_east = sq2bb(sq) << 2,
           castle_west = sq2bb(sq) >> 2;
  if (castle_east & meta.castling_rights &&
      !is_square_checked(own, other, other_pieces, other_pawn_attacks, sq + 1) &&
      !(occ & (sq2bb(sq + 1) | sq2bb(sq + 2))))
  {
    move_buffer_append_move(sq, sq + 2, PT_NONE, MT_CASTLE_KING, out);
  }
  if (castle_west & meta.castling_rights &&
      !is_square_checked(own, other, other_pieces, other_pawn_attacks, sq - 1) &&
      !(occ & (sq2bb(sq - 1) | sq2bb(sq - 2) | sq2bb(sq - 3))))
  {
    move_buffer_append_move(sq, sq - 2, PT_NONE, MT_CASTLE_QUEEN, out);
  }
}

static const bitboard rank_1 = 0x00000000000000FF;
static const bitboard rank_4 = 0x00000000FF000000;
static const bitboard rank_5 = 0x000000FF00000000;
static const bitboard rank_8 = 0xFF00000000000000;
static const bitboard a_file = 0x0101010101010101;
static const bitboard h_file = 0x8080808080808080;

static inline void
move_buffer_append_promotions(square from, square to, piece_type capture, move_buffer *out)
{
  move_buffer_append_move(from, to, capture, MT_PROMOTION_KNIGHT, out);
  move_buffer_append_move(from, to, capture, MT_PROMOTION_BISHOP, out);
  move_buffer_append_move(from, to, capture, MT_PROMOTION_ROOK, out);
  move_buffer_append_move(from, to, capture, MT_PROMOTION_QUEEN, out);
}

static inline void
generate_pawn_moves_white(bitboard own, bitboard other, bitboard pieces, piece_type types[NUM_SQUARES], square en_passant_potential, move_buffer *out)
{
  bitboard occ = own | other,
           singles = (pieces  << 0x8) & ~occ,
           doubles = (singles << 0x8) & ~occ & rank_4,
           east_captures = (pieces << 0x9) & ~a_file & (other | ep2bb(en_passant_potential)),
           west_captures = (pieces << 0x7) & ~h_file & (other | ep2bb(en_passant_potential));

  square from, to;

  while (singles)
  {
    to = pop_bit(&singles);
    from = to - 0x8;
    if (sq2bb(to) & ~rank_8) // no promotion
    {
      move_buffer_append_move(from, to, PT_NONE, MT_NORMAL, out);
    }
    else // promotion
    {
      move_buffer_append_promotions(from, to, PT_NONE, out);
    }
  }
  while (doubles)
  {
    to = pop_bit(&doubles);
    from = to - 0x10;
    move_buffer_append_move(from, to, PT_NONE, MT_DOUBLE_PAWN, out);
  }
  while (east_captures)
  {
    to = pop_bit(&east_captures);
    from = to - 0x9;
    if (sq2bb(to) & ~rank_8) // no promotion
    {
      enum MOVE_TYPE mt = (types[to] == PT_NONE) ? MT_EN_PASSANT : MT_NORMAL;
      move_buffer_append_move(from, to, types[to], mt, out);
    }
    else // promotion
    {
      move_buffer_append_promotions(from, to, types[to], out);
    }
  }
  while (west_captures)
  {
    to = pop_bit(&west_captures);
    from = to - 0x7;
    if (sq2bb(to) & ~rank_8) // no promotion
    {
      enum MOVE_TYPE mt = (types[to] == PT_NONE) ? MT_EN_PASSANT : MT_NORMAL;
      move_buffer_append_move(from, to, types[to], mt, out);
    }
    else // promotion
    {
      move_buffer_append_promotions(from, to, types[to], out);
    }
  }
}

static inline void
generate_pawn_moves_black(bitboard own, bitboard other, bitboard pieces, piece_type types[NUM_SQUARES], square en_passant_potential, move_buffer *out)
{
  bitboard occ = own | other,
           singles = (pieces  >> 0x8) & ~occ,
           doubles = (singles >> 0x8) & ~occ & rank_5,
           east_captures = (pieces >> 0x7) & ~a_file & (other | ep2bb(en_passant_potential)),
           west_captures = (pieces >> 0x9) & ~h_file & (other | ep2bb(en_passant_potential));

  square from, to;

  while (singles)
  {
    to = pop_bit(&singles);
    from = to + 0x8;
    if (sq2bb(to) & ~rank_1) // no promotion
    {
      move_buffer_append_move(from, to, PT_NONE, MT_NORMAL, out);
    }
    else // promotion
    {
      move_buffer_append_promotions(from, to, PT_NONE, out);
    }
  }
  while (doubles)
  {
    to = pop_bit(&doubles);
    from = to + 0x10;
    move_buffer_append_move(from, to, PT_NONE, MT_DOUBLE_PAWN, out);
  }
  while (east_captures)
  {
    to = pop_bit(&east_captures);
    from = to + 0x7;
    if (sq2bb(to) & ~rank_1) // no promotion
    {
      enum MOVE_TYPE mt = (types[to] == PT_NONE) ? MT_EN_PASSANT : MT_NORMAL;
      move_buffer_append_move(from, to, types[to], mt, out);
    }
    else // promotion
    {
      move_buffer_append_promotions(from, to, types[to], out);
    }
  }
  while (west_captures)
  {
    to = pop_bit(&west_captures);
    from = to + 0x9;
    if (sq2bb(to) & ~rank_1) // no promotion
    {
      enum MOVE_TYPE mt = (types[to] == PT_NONE) ? MT_EN_PASSANT : MT_NORMAL;
      move_buffer_append_move(from, to, types[to], mt, out);
    }
    else // promotion
    {
      move_buffer_append_promotions(from, to, types[to], out);
    }
  }
}


size_t
generate_moves(game_state *game, move_buffer *out)
{
  board_state *board = &game->dynamics.board;
  color color_own = game->dynamics.active,
        color_other = OTHER_COLOR(color_own);
  bitboard *own = board->bitboards + color_own,
           *other = board->bitboards + color_other;
  bitboard own_union = own[PR_P] | own[PR_N] | own[PR_B] | own[PR_R] | own[PR_Q] | own[PR_K];
  bitboard other_union = other[PR_P] | other[PR_N] | other[PR_B] | other[PR_R] | other[PR_Q] | other[PR_K];

  bitboard copy;

  out->size = 0;
  out->sorted = 0;

#define GENERATE_ALL_MOVES(PT, own, other, pieces, types, out) \
  copy = pieces; \
  while (copy) \
  { \
    square from = pop_bit(&copy); \
    generate_## PT ##_moves(own, other, from, types, out); \
  }
  // generate sliding moves
  GENERATE_ALL_MOVES(bishop, own_union, other_union, own[PR_B], board->types, out);
  GENERATE_ALL_MOVES(rook, own_union, other_union, own[PR_R], board->types, out);
  GENERATE_ALL_MOVES(queen, own_union, other_union, own[PR_Q], board->types, out);

  GENERATE_ALL_MOVES(knight, own_union, other_union, own[PR_N], board->types, out);

#undef GENERATE_ALL_MOVES

  bitboard other_pawn_attacks;
  if (color_own == COLOR_WHITE) // white's move
  {
    generate_pawn_moves_white(own_union, other_union, board->bitboards[PT_WP], board->types, game->statics.en_passant_potential, out);
    other_pawn_attacks  = (other[PR_P] >> 9) & ~h_file;
    other_pawn_attacks |= (other[PR_P] >> 7) & ~a_file;
    // TODO: may fail if king dead
    generate_king_moves(own_union, other_union, other, other_pawn_attacks, log_bit(own[PR_K]), board->types, game->statics, out);
  }
  else // black's move
  {
    generate_pawn_moves_black(own_union, other_union, board->bitboards[PT_BP], board->types, game->statics.en_passant_potential, out);
    other_pawn_attacks  = (other[PR_P] << 7) & ~h_file;
    other_pawn_attacks |= (other[PR_P] << 9) & ~a_file;
    generate_king_moves(own_union, other_union, other, other_pawn_attacks, log_bit(own[PR_K]), board->types, game->statics, out);
  }

  return out->size;
}

move_t *
move_buffer_next(move_buffer *mbuf)
{
  size_t i, max, start;
  move_t *moves, swap;

  start = mbuf->sorted;
  moves = mbuf->moves;

  if (start >= mbuf->size)
    return NULL;

  max = start;

  for (i = start + 1; i < mbuf->size; ++i)
  {
    if (moves[i].heuristic > moves[max].heuristic)
      max = i;
  }

  swap = moves[start];
  moves[start] = moves[max];
  moves[max] = swap;
  ++mbuf->sorted;

  return &moves[start];
}


int
king_safety(board_state *board, color active)
{
  bitboard wocc = board->bitboards[PT_WP] | board->bitboards[PT_WN] | board->bitboards[PT_WB] | board->bitboards[PT_WR] | board->bitboards[PT_WQ] | board->bitboards[PT_WK];
  bitboard bocc = board->bitboards[PT_BP] | board->bitboards[PT_BN] | board->bitboards[PT_BB] | board->bitboards[PT_BR] | board->bitboards[PT_BQ] | board->bitboards[PT_BK];
  bitboard wpawn_attacks, bpawn_attacks;

  wpawn_attacks  = (board->bitboards[PT_WP] << 9) & ~a_file;
  wpawn_attacks |= (board->bitboards[PT_WP] << 7) & ~h_file;
  bpawn_attacks  = (board->bitboards[PT_BP] >> 9) & ~h_file;
  bpawn_attacks |= (board->bitboards[PT_BP] >> 7) & ~a_file;

  switch (active)
  {
  case COLOR_WHITE:
    return !is_square_checked(wocc, bocc, &board->bitboards[COLOR_BLACK], bpawn_attacks, log_bit(board->bitboards[PT_WK]));
  case COLOR_BLACK:
    return !is_square_checked(bocc, wocc, &board->bitboards[COLOR_WHITE], wpawn_attacks, log_bit(board->bitboards[PT_BK]));
  }

  return 0;
}
