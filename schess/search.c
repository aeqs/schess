#include <schess/eval.h>
#include <schess/gen.h>
#include <schess/move.h>
#include <schess/search.h>
#include <schess/time.h>
#include <schess/tt.h>
#include <schess/types.h>
#include <schess/utils.h>
#include <stddef.h>
#include <stdlib.h>

const struct search_limits search_limits_infinite =
{
  .depth = ULONG_MAX,
  .mate  = ULONG_MAX,
  .nodes = ULONG_MAX,
  .time  = ULONG_MAX,
};

struct search_limits_internal
{
  unsigned long depth_main, depth_qs, *nodes;
};

#define QUIES_DEPTH 10
int
quiescence(game_state *game, int alpha, int beta, struct search_state sstate, struct search_limits_internal limits)
{
  int best_score, score = 0;
  irreversible_state statics_copy;
  int mate, is_quiesce;
  move_buffer mbuf;
  move_t *m;
  struct tt_entry *entry;
  enum tt_entry_type entry_type = TT_EXACT;

  *limits.nodes -= 1;

  entry = tt_probe(sstate.qstt, game->statics.zhash);
  if (entry && entry->depth >= limits.depth_main &&
      ((entry->type == TT_EXACT) ||
       (entry->type == TT_ALPHA && entry->score <= alpha) ||
       (entry->type == TT_BETA  && entry->score >= beta)))
  {
    return entry->score;
  }

  //best_score = in_check ? -oo : eval_position(game, meta);
  best_score = eval_position(game);
  if (!limits.depth_qs)
    goto store_exit;

  if (best_score >= beta)
  {
    entry_type = TT_BETA;
    goto store_exit;
  }
  if (best_score > alpha)
  {
    alpha = best_score;
    entry_type = TT_ALPHA;
  }

  limits.depth_qs -= 1;

  statics_copy = game->statics;
  generate_moves(game, &mbuf);
  while ((m = move_buffer_next(&mbuf)) != NULL && !sc_test(sstate.sc) && *limits.nodes)
  {
    /*
    is_quiesce = m->capture == PT_NONE &&                                             // no capture
                 king_safety(&game->board, game->active) &&                           // no check
                 !(in_check && king_safety(&game->board, OTHER_COLOR(game->active))); // no check evasion
    */
    is_quiesce = m->capture == PT_NONE;

    if (is_quiesce)
      continue;

    mate = move_make(m, game);
    if (mate)
      score = mate;
    else
      score = -quiescence(game, -beta, -alpha, sstate, limits);
    move_unmake(m, game);
    game->statics = statics_copy;

    if (score >= beta)
    {
      best_score = score;
      entry_type = TT_BETA;
      break;
    }
    if (score > best_score)
    {
      best_score = score;
      if (score > alpha)
      {
        entry_type = TT_ALPHA;
        alpha = score;
      }
    }
  }

  limits.depth_qs += 1;

store_exit:

  tt_store(sstate.qstt, (struct tt_entry)
      {
      .depth = limits.depth_main,
      .zhash = statics_copy.zhash,
      .type  = entry_type,
      .score = best_score,
      });

  return best_score;
}

static int
alpha_beta(game_state *game, int alpha, int beta, struct search_state sstate, struct search_limits_internal limits)
{
  int best_score = -oo, score = 0;
  irreversible_state statics_copy;
  int mate;
  struct tt_entry *entry;
  enum tt_entry_type entry_type;
  move_buffer mbuf;
  move_t best, *m;

  if (!limits.depth_main)
    return quiescence(game, alpha, beta, sstate, limits);

  // TODO: test performance later
  //_mm_prefetch((char *) &tt->entries[meta.zhash & tt->mask], _MM_HINT_NTA);

  entry = tt_probe(sstate.tt, game->statics.zhash);
  if (entry && entry->depth >= limits.depth_main &&
      ((entry->type == TT_EXACT) ||
       (entry->type == TT_ALPHA && entry->score <= alpha) ||
       (entry->type == TT_BETA  && entry->score >= beta)))
  {
    return entry->score;
  }

  *limits.nodes -= 1;
  limits.depth_main -= 1;

  entry_type = TT_EXACT;

  statics_copy = game->statics;
  generate_moves(game, &mbuf);
  while ((m = move_buffer_next(&mbuf)) != NULL && !sc_test(sstate.sc) && *limits.nodes)
  {
    mate = move_make(m, game);
    if (mate)
      score = mate;
    else
      score = -alpha_beta(game, -beta, -alpha, sstate, limits);
    move_unmake(m, game);
    game->statics = statics_copy;

    if (score >= beta)
    {
      best_score = score;
      best = *m;
      entry_type = TT_BETA;
      break;
    }
    if (score > best_score)
    {
      best_score = score;
      best = *m;
      if (score > alpha)
      {
        entry_type = TT_ALPHA;
        alpha = score;
      }
    }
  }

  tt_store(sstate.tt, (struct tt_entry)
      {
      .depth = limits.depth_main + 1,
      .zhash = statics_copy.zhash,
      .type  = entry_type,
      .score = best_score,
      .best  = best,
      });

  return best_score;
}

struct search_result
search_root(game_state *game, struct search_state sstate, struct search_limits limits)
{
  int alpha, beta, delta, score, is_mate, mate_limit_set;
  unsigned depth, window_size;
  struct tt_entry best;
  struct search_limits_internal limits_internal;
  struct timespec time;
  unsigned long nodes_before = limits.nodes;

  mate_limit_set = limits.mate != ULONG_MAX;

  limits_internal.depth_main = limits.depth;
  limits_internal.depth_qs   = mate_limit_set ? 0 : 20; // TODO: avoid cycles
  limits_internal.nodes      = &limits.nodes;
  if (mate_limit_set)
    limits.mate = (2 * limits.mate) + 1;

  time.tv_sec = limits.time / 1000;
  time.tv_nsec = (limits.time % 1000) * 1000000;

  sc_timeout(sstate.sc, time);

  delta = 50;

  score       = -oo;
  window_size = delta;
  best        = (struct tt_entry) { };
  is_mate     = 0;

  for (depth = 1; depth <= limits.depth && depth <= limits.mate; ++depth)
  {
    limits_internal.depth_main = depth;
    alpha = 0 - window_size;
    beta  = 0 + window_size;

    while (1)
    {
      score = alpha_beta(game, alpha, beta, sstate, limits_internal);

      if (score <= alpha)
      {
        beta  = (alpha + beta) / 2;
        alpha = score - delta;
      }
      else if (score >= beta)
      {
        beta = score + delta;
      }
      else break;
    }

    if (sc_test(sstate.sc))
      break;

    // always set at end of root call to alpha_beta()
    best = *tt_probe(sstate.tt, game->statics.zhash);

    if (best.score >= (oo / 2))
    {
      is_mate = 1;
      break;
    }
  }

  if (best.depth > limits.mate || (mate_limit_set && !is_mate))
    return (struct search_result) { };

  return (struct search_result)
  {
    .move  = best.best,
    .depth = best.depth,
    .nodes = nodes_before - limits.nodes,
    .score = best.score,
  };
}
