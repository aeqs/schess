#include <ctype.h>
#include <schess/gen.h>
#include <schess/move.h>
#include <schess/types.h>
#include <schess/utils.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
error(int status, const char *msg)
{
  if (msg != NULL)
    fprintf(stderr, "%s\n", msg);

  if (status)
    exit(status);
}

int
str_match(const char **str, const char *cmp)
{
  size_t len;

  len = strlen(cmp);
  if (strncmp(*str, cmp, len))
    return 0;

  *str += len;
  return 1;
}

const char *
str_next_nonspace(const char *str)
{
  while (isspace(*str))
    ++str;

  return str;
}

unsigned
str_skip_blanks(const char **str)
{
  unsigned num;

  for (num = 0; isblank(**str); ++num)
    ++*str;

  return num;
}

static const char *move_names[9] =
{
  "  ",
  "DP",
  "EP",
  "CK",
  "CQ",
  "=N",
  "=B",
  "=R",
  "=Q",
};
const char *
move_name(enum MOVE_TYPE mt)
{
  return move_names[mt];
}

static const char *piece_names[PT_COUNT] =
{
  " ",
  "P", "N", "B", "R", "Q", "K",
  "p", "n", "b", "r", "q", "k",
};
const char *
piece_name(piece_type pt)
{
  return piece_names[pt];
}

static const char *square_names[NUM_SQUARES] =
{
  "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",
  "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
  "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
  "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
  "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
  "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
  "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
  "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
};
const char *
square_name(square sq)
{
  return square_names[sq];
}

static int
parse_board(const char *board_string, board_state *board_out, const char **end_ptr)
{
  size_t string_pos;
  square board_pos;
  board_state board = { 0 };

  char c;
  unsigned file, rank;
  square sq;

  // error catching variables
  unsigned last_slash = 0;
  size_t last_digit = 71; // max board_string size

  for (string_pos = 0, board_pos = a1; board_pos < NUM_SQUARES; ++string_pos)
  {
    c = board_string[string_pos];

    switch (c)
    {

#define PARSE_BOARD_REGISTER_PIECES_CASE(lit, PT) \
  case (lit): \
    { \
      file = board_pos & BITMASK(3); \
      rank = 7 - (board_pos >> 3); \
      sq   = (rank * 8) + file; \
      board.bitboards[PT] |= sq2bb(sq); \
      board.types[sq] = PT; \
      ++board_pos; \
      break; \
    }
      PARSE_BOARD_REGISTER_PIECES_CASE('P', PT_WP);
      PARSE_BOARD_REGISTER_PIECES_CASE('N', PT_WN);
      PARSE_BOARD_REGISTER_PIECES_CASE('B', PT_WB);
      PARSE_BOARD_REGISTER_PIECES_CASE('R', PT_WR);
      PARSE_BOARD_REGISTER_PIECES_CASE('Q', PT_WQ);
      PARSE_BOARD_REGISTER_PIECES_CASE('K', PT_WK);
      PARSE_BOARD_REGISTER_PIECES_CASE('p', PT_BP);
      PARSE_BOARD_REGISTER_PIECES_CASE('n', PT_BN);
      PARSE_BOARD_REGISTER_PIECES_CASE('b', PT_BB);
      PARSE_BOARD_REGISTER_PIECES_CASE('r', PT_BR);
      PARSE_BOARD_REGISTER_PIECES_CASE('q', PT_BQ);
      PARSE_BOARD_REGISTER_PIECES_CASE('k', PT_BK);
#undef PARSE_BOARD_REGISTER_PIECES_CASE

    case '/':
      // invalid slash position
      if (board_pos != last_slash + 8) return 2;

      last_slash = board_pos;
      break;
    default:
      if (c > '0' && c <= '8')
      {
        // two digits in a row
        if (string_pos == last_digit + 1) return 3;

        last_digit = string_pos;
        board_pos += c - '0';
        break;
      }
      // undefined character
      return 1;
    }

    // missing slash
    if (board_pos > last_slash + 8) return 4;
  }

  *board_out = board;
  *end_ptr = &board_string[string_pos];
  return 0;
}

static int
parse_active(const char *active_string, color *color_out, const char **end_ptr)
{
  switch (*active_string)
  {
  case 'w':
    *color_out = COLOR_WHITE;
    break;
  case 'b':
    *color_out = COLOR_BLACK;
    break;
  default:
    return 1;
  }

  *end_ptr = &active_string[1];
  return 0;
}

static int
parse_castling(const char *castling_string, bitboard *castling_out, const char **end_ptr)
{
  size_t string_pos;
  bitboard castling_rights;

  string_pos = 0;
  castling_rights = BB_EMPTY;

  if (castling_string[string_pos] == '-')
  {
    *castling_out = castling_rights;
    *end_ptr = &castling_string[1];
    return 0;
  }

  if (castling_string[string_pos] == 'K')
  {
    castling_rights |= 0x0000000000000040;
    ++string_pos;
  }
  if (castling_string[string_pos] == 'Q')
  {
    castling_rights |= 0x0000000000000004;
    ++string_pos;
  }
  if (castling_string[string_pos] == 'k')
  {
    castling_rights |= 0x4000000000000000;
    ++string_pos;
  }
  if (castling_string[string_pos] == 'q')
  {
    castling_rights |= 0x0400000000000000;
    ++string_pos;
  }

  // no valid character found
  if (!string_pos) return 1;

  *castling_out = castling_rights;
  *end_ptr = &castling_string[string_pos];
  return 0;
}

static int
is_valid_square_name(char file, char rank)
{
  return (file >= 'a' && file <= 'h' &&
          rank >= '1' && rank <= '8');
}
square
square_from_name(char file, char rank)
{
  return ((rank - '1') * 8) + (file - 'a');
}
static int
parse_en_passant(const char *en_passant_string, square *en_passant_potential_out, const char **end_ptr)
{
  if (en_passant_string[0] == '-')
  {
    *en_passant_potential_out = EP_NO_POTENTIAL;
    *end_ptr = &en_passant_string[1];
    return 0;
  }

  if (!is_valid_square_name(en_passant_string[0], en_passant_string[1]))
      return 1;

  *en_passant_potential_out = square_from_name(en_passant_string[0], en_passant_string[1]);
  *end_ptr = &en_passant_string[2];
  return 0;
}

static int
parse_halfmove_clock(const char *halfmove_clock_string, unsigned *halfmove_clock_out, const char **end_ptr)
{
  if (halfmove_clock_string[0] < '0' || halfmove_clock_string[0] > '9') return 1;
  *halfmove_clock_out = halfmove_clock_string[0] - '0';
  *end_ptr = &halfmove_clock_string[1];

  if (halfmove_clock_string[1] < '0' || halfmove_clock_string[1] > '9') return 0;
  *halfmove_clock_out *= 10;
  *halfmove_clock_out += halfmove_clock_string[1] - '0';
  *end_ptr = &halfmove_clock_string[2];

  return 0;
}

static int
parse_fullmove_number(const char *fullmove_number_string, unsigned *fullmove_number_out, const char **end_ptr)
{
  size_t i;

  if (fullmove_number_string[0] < '0' || fullmove_number_string[0] > '9') return 1;
  *fullmove_number_out = fullmove_number_string[0] - '0';
  *end_ptr = &fullmove_number_string[1];

  for (i = 1; i < 4; ++i) // max 4 digit fullmove number
  {
    if (fullmove_number_string[i] < '0' || fullmove_number_string[i] > '9') break;
    *fullmove_number_out *= 10;
    *fullmove_number_out += fullmove_number_string[i] - '0';
    *end_ptr = &fullmove_number_string[i + 1];
  }

  return 0;
}

int
parse_FEN(const char *FEN, game_state *game_out, const char **end_ptr)
{
  game_state game = { 0 };
  const char *str = FEN, *old_str = str;
  int err;

  str_skip_blanks(&str);
  err = parse_board(str, &game.dynamics.board, &str);
  if (err) return err;
  if (!str_skip_blanks(&str)) return 1;

  err = parse_active(str, &game.dynamics.active, &str);
  if (err) return err;
  if (!str_skip_blanks(&str)) return 1;

  err = parse_castling(str, &game.statics.castling_rights, &str);
  if (err) return err;
  if (!str_skip_blanks(&str)) return 1;

  err = parse_en_passant(str, &game.statics.en_passant_potential, &str);
  if (err) return err;

  // optional fields
  do
  {
    old_str = str;
    if (!str_skip_blanks(&str)) break;

    err = parse_halfmove_clock(str, &game.statics.halfmove_clock, &str);
    if (err) break;
    old_str = str;
    if (!str_skip_blanks(&str)) break;

    err = parse_fullmove_number(str, &game.dynamics.fullmove, &str);
    if (err) break;
  } while (0);

  if (game_out)
    *game_out = game;
  if (end_ptr)
    *end_ptr = old_str;
  return 0;
}

void
print_board(board_state *board)
{
  size_t r, c;
  piece_type type;

  for (r = 0; r < 8; ++r)
  {
    printf("+---+---+---+---+---+---+---+---+\n|");
    for (c = 0; c < 8; ++c)
    {
      type = board->types[((7 - r) * 8) + c];
      printf(" %s |", piece_names[type]);
    }
    printf("\n");
  }
  printf("+---+---+---+---+---+---+---+---+\n");
}

void
print_moves(board_state *board, move_buffer *mbuf)
{
  size_t i;

  for (i = 0; i < mbuf->size; ++i)
  {
    move_t m = mbuf->moves[i];
    printf("[%3zu]:  ", i);
    print_move(board, m);
    printf("\n");
  }
}

void
print_move(board_state *board, move_t m)
{
  printf("(%s) %s -> %s (%s) %s",
      piece_names[board->types[m.from]],
      square_names[m.from], square_names[m.to],
      piece_names[m.capture], move_names[m.type]);
}

static enum PIECE_REL
piece_from_symbol(const char name)
{
  switch (name) {
  case 'N': return PR_N;
  case 'B': return PR_B;
  case 'R': return PR_R;
  case 'Q': return PR_Q;
  case 'K': return PR_K;
  }
  return PR_P;
}


// TODO: fix everything vs game_state -> reversible_state
static inline int
parse_pawn_push(const char *SAN, game_state *game, move_t *move_out)
{
  const piece_type *types = game->dynamics.board.types;
  move_t res = { .capture = PT_NONE };

  if (!is_valid_square_name(SAN[0], SAN[1]))
    return 1;

  res.to = square_from_name(SAN[0], SAN[1]);

  switch (game->dynamics.active)
  {
  case COLOR_WHITE:
    if (res.to >= 8 && types[res.to - 8] == PT_WP)
    {
      res.from = res.to - 8;
      res.type = MT_NORMAL;
    }
    else if (res.to >= 16 && types[res.to - 16] == PT_WP)
    {
      res.from = res.to - 16;
      res.type = MT_DOUBLE_PAWN;
    }
    else
      return 1;
    break;

  case COLOR_BLACK:
    if (res.to < (NUM_SQUARES - 8) && types[res.to + 8] == PT_BP)
    {
      res.from = res.to + 8;
      res.type = MT_NORMAL;
    }
    else if (res.to < (NUM_SQUARES - 16) && types[res.to + 16] == PT_BP)
    {
      res.from = res.to + 16;
      res.type = MT_DOUBLE_PAWN;
    }
    else
      return 1;
    break;

  default:
    return 1;
  }

  if (move_out)
    *move_out = res;
  return 0;
}

static inline int
parse_pawn_capture_no_promo(const char *SAN, game_state *game, move_t *move_out)
{
  // SAN[0] should contain from-file
  // SAN[1] should contain 'x'

  const piece_type *types = game->dynamics.board.types;
  move_t res = { .type = MT_NORMAL, .capture = PT_NONE };

  if (!is_valid_square_name(SAN[2], SAN[3]) ||
      !is_valid_square_name(SAN[0], SAN[3]) ||
      SAN[1] != 'x')
    return 1;

  res.to   = square_from_name(SAN[2], SAN[3]);
  res.from = square_from_name(SAN[0], SAN[3]);

  switch (game->dynamics.active)
  {
  case COLOR_WHITE:
    if (res.from < 8)
      return 1;

    res.from -= 8;
    if (types[res.from] != PT_WP)
      return 1;
    break;

  case COLOR_BLACK:
    if (res.from >= (NUM_SQUARES - 8))
      return 1;

    res.from += 8;
    if (types[res.from] != PT_BP)
      return 1;
    break;
  }

  res.capture = types[res.to];
  if (COLOR_OF(res.capture) == game->dynamics.active)
    return 1;

  if (res.capture == PT_NONE)
  {
    // TODO: test ep (add meta to args)
    //if (res.to != meta->en_passant_potential)
    //  return 1;

    res.type = MT_EN_PASSANT;
  }

  if (move_out)
    *move_out = res;
  return 0;
}

static inline int
parse_pawn_push_promo(const char *SAN, game_state *game, move_t *move_out)
{
  const piece_type *types = game->dynamics.board.types;
  move_t res;

  if (!is_valid_square_name(SAN[0], SAN[1]) || SAN[2] != '=')
    return 1;

  res.to = square_from_name(SAN[0], SAN[1]);
  if (types[res.to] != PT_NONE)
    return 1;
  res.capture = PT_NONE;

  switch (game->dynamics.active)
  {
  case COLOR_WHITE:
    if (res.to < 8)
      return 1;

    res.from = res.to - 8;
    if (types[res.from] != PT_WP)
      return 1;
    break;

  case COLOR_BLACK:
    if (res.to >= (NUM_SQUARES - 8))
      return 1;

    res.from = res.to + 8;
    if (types[res.from] != PT_BP)
      return 1;
    break;
  }

  switch (piece_from_symbol(SAN[3]))
  {
  case PR_P:
    return 1;
  case PR_N:
    res.type = MT_PROMOTION_KNIGHT;
    break;
  case PR_B:
    res.type = MT_PROMOTION_BISHOP;
    break;
  case PR_R:
    res.type = MT_PROMOTION_ROOK;
    break;
  case PR_Q:
    res.type = MT_PROMOTION_QUEEN;
    break;
  case PR_K:
    return 1;
  }

  if (move_out)
    *move_out = res;
  return 0;
}

static inline int
parse_pawn_capture_promo(const char *SAN, game_state *game, move_t *move_out)
{
  const piece_type *types = game->dynamics.board.types;
  move_t res;

  if (!is_valid_square_name(SAN[2], SAN[3]) ||
      !is_valid_square_name(SAN[0], SAN[3]) ||
      SAN[1] != 'x' || SAN[4] != '=')
    return 1;

  res.to   = square_from_name(SAN[2], SAN[3]);
  res.from = square_from_name(SAN[0], SAN[3]);

  switch (game->dynamics.active)
  {
  case COLOR_WHITE:
    if (res.to < 8)
      return 1;

    res.from = res.to - 8;
    if (types[res.from] != PT_WP)
      return 1;
    break;

  case COLOR_BLACK:
    if (res.to >= (NUM_SQUARES - 8))
      return 1;

    res.from = res.to + 8;
    if (types[res.from] != PT_BP)
      return 1;
    break;
  }

  res.capture = types[res.to];
  if (COLOR_OF(res.capture) != OTHER_COLOR(game->dynamics.active))
    return 1;

  switch (piece_from_symbol(SAN[5]))
  {
  case PR_P:
    return 1;
  case PR_N:
    res.type = MT_PROMOTION_KNIGHT;
    break;
  case PR_B:
    res.type = MT_PROMOTION_BISHOP;
    break;
  case PR_R:
    res.type = MT_PROMOTION_ROOK;
    break;
  case PR_Q:
    res.type = MT_PROMOTION_QUEEN;
    break;
  case PR_K:
    return 1;
  }

  if (move_out)
    *move_out = res;
  return 0;
}

static inline int
parse_pawn_move(const char *SAN, size_t len, game_state *game, move_t *move_out)
{
  int err;
  move_t res;

  switch (len)
  {
  case 2: // push, e.g. 'e4'
    err = parse_pawn_push(SAN, game, &res);
    break;
  case 4:
    if (SAN[1] == 'x') // capture, e.g. 'exd5'
      err = parse_pawn_capture_no_promo(SAN, game, &res);
    else // no capture, promotion, e.g. 'e8=Q'
      err = parse_pawn_push_promo(SAN, game, &res);
    break;
  case 6: // capture, promotion, e.g. 'exd8=Q'
    err = parse_pawn_capture_promo(SAN, game, &res);
    break;
  default:
    return 1;
  }

  if (err)
    return err;

  if (!post_move_king_safety(&res, game))
    return 1;

  if (move_out)
    *move_out = res;
  return 0;
}

static inline int
parse_piece_silent(const char *SAN, game_state *game, piece_type piece, move_buffer *mbuf, move_t *move_out)
{
  // SAN[0] contains the moving piece type

  size_t i;
  move_t m;
  square to;

  if (!is_valid_square_name(SAN[1], SAN[2]))
    return 1;

  to = square_from_name(SAN[1], SAN[2]);

  for (i = 0; i < mbuf->size; ++i)
  {
    m = mbuf->moves[i];

    if (m.to != to ||
        game->dynamics.board.types[m.from] != piece ||
        game->dynamics.board.types[m.to]   != PT_NONE)
      continue;

    if (post_move_king_safety(&m, game))
    {
      move_out->from = m.from;
      move_out->to = to;
      move_out->capture = PT_NONE;
      move_out->type = MT_NORMAL;
      return 0;
    }
  }

  return 1;
}

static inline int
parse_piece_capture(const char *SAN, game_state *game, square to, piece_type piece, move_buffer *mbuf, move_t *move_out)
{
  // SAN[0] contains the moving piece type
  // SAN[1] contains 'x'
  (void) SAN;

  size_t i;
  move_t m;

  for (i = 0; i < mbuf->size; ++i)
  {
    m = mbuf->moves[i];
    if (m.to != to ||
        game->dynamics.board.types[m.from] != piece ||
        game->dynamics.board.types[m.to]   == PT_NONE)
      continue;

    if (post_move_king_safety(&m, game))
    {
      move_out->from    = m.from;
      move_out->to      = to;
      move_out->capture = game->dynamics.board.types[to]; // TODO: maybe test team
      move_out->type    = MT_NORMAL;
      return 0;
    }
  }

  return 1;
}

static inline int
parse_piece_silent_file_hint(const char *SAN, game_state *game, square to, piece_type piece, move_buffer *mbuf, move_t *move_out)
{
  // SAN[0] contains the moving piece type
  // SAN[1] contains valid file identifier

  size_t i;
  move_t m;

  for (i = 0; i < mbuf->size; ++i)
  {
    m = mbuf->moves[i];
    if (m.to != to || (m.from >> 3) != (unsigned) SAN[1] - 'a' ||
        game->dynamics.board.types[m.from] != piece)
      continue;

    if (post_move_king_safety(&m, game))
    {
      move_out->from    = m.from;
      move_out->to      = to;
      move_out->capture = game->dynamics.board.types[to]; // TODO: maybe test team
      move_out->type    = MT_NORMAL;
      return 0;
    }
  }

  return 1;
}

static inline int
parse_piece_silent_rank_hint(const char *SAN, game_state *game, square to, piece_type piece, move_buffer *mbuf, move_t *move_out)
{
  // SAN[0] contains the moving piece type
  // SAN[1] contains valid rank identifier

  size_t i;
  move_t m;

  for (i = 0; i < mbuf->size; ++i)
  {
    m = mbuf->moves[i];
    if (m.to != to || (m.from & BITMASK(3)) != (unsigned) SAN[1] - '1' ||
        game->dynamics.board.types[m.from] != piece)
      continue;

    if (post_move_king_safety(&m, game))
    {
      move_out->from    = m.from;
      move_out->to      = to;
      move_out->capture = game->dynamics.board.types[to]; // TODO: maybe test team
      move_out->type    = MT_NORMAL;
      return 0;
    }
  }

  return 1;
}

static inline int
parse_piece_capture_file_hint(const char *SAN, game_state *game, square to, piece_type piece, move_buffer *mbuf, move_t *move_out)
{
  // SAN[0] contains the moving piece type
  // SAN[1] contains valid file identifier
  // SAN[2] contains 'x'

  size_t i;
  move_t m;

  for (i = 0; i < mbuf->size; ++i)
  {
    m = mbuf->moves[i];
    if (m.to != to || (m.from >> 3) != (unsigned) SAN[1] - 'a' ||
        game->dynamics.board.types[m.from] != piece)
      continue;

    if (post_move_king_safety(&m, game))
    {
      move_out->from    = m.from;
      move_out->to      = to;
      move_out->capture = game->dynamics.board.types[to]; // TODO: maybe test team
      move_out->type    = MT_NORMAL;
      return 0;
    }
  }

  return 1;
}

static inline int
parse_piece_capture_rank_hint(const char *SAN, game_state *game, square to, piece_type piece, move_buffer *mbuf, move_t *move_out)
{
  // SAN[0] contains the moving piece type
  // SAN[1] contains valid rank identifier
  // SAN[2] contains 'x'

  size_t i;
  move_t m;

  for (i = 0; i < mbuf->size; ++i)
  {
    m = mbuf->moves[i];
    if (m.to != to || (m.from & BITMASK(3)) != (unsigned) SAN[1] - '1' ||
        game->dynamics.board.types[m.from] != piece)
      continue;

    if (post_move_king_safety(&m, game))
    {
      move_out->from    = m.from;
      move_out->to      = to;
      move_out->capture = game->dynamics.board.types[to]; // TODO: maybe test team
      move_out->type    = MT_NORMAL;
      return 0;
    }
  }

  return 1;
}

static int
parse_piece_move(const char *SAN, size_t len, game_state *game, move_t *move_out)
{
  move_buffer mbuf[1];
  piece_type piece;
  move_t res = { .type = MT_NORMAL };
  int err;

  if (piece_from_symbol(SAN[0]) == PR_P || len == 2) return 1;
  piece = game->dynamics.active + piece_from_symbol(SAN[0]);

  generate_moves(game, mbuf);

  if (!is_valid_square_name(SAN[len - 2], SAN[len - 1]))
    return 1;

  res.to = square_from_name(SAN[len - 2], SAN[len - 1]);

  switch (len)
  {
  case 3:
    err = parse_piece_silent(SAN, game, piece, mbuf, &res);
    break;
  case 4:
    if (SAN[1] == 'x') // capture, disambiguous
    {
      err = parse_piece_capture(SAN, game, res.to, piece, mbuf, &res);
    }
    else // piece move, half ambiguous
    {
      if (SAN[1] >= 'a' && SAN[1] <= 'h') // file hint
        err = parse_piece_silent_file_hint(SAN, game, res.to, piece, mbuf, &res);
      else if (SAN[1] >= '1' && SAN[1] <= '8') // rank hint
        err = parse_piece_silent_rank_hint(SAN, game, res.to, piece, mbuf, &res);
      else
        return 1;
    }
    break;
  case 5:
    if (SAN[2] == 'x') // capture, half disambiguous
    {
      if (SAN[1] >= 'a' && SAN[1] <= 'h') // file hint
        err = parse_piece_capture_file_hint(SAN, game, res.to, piece, mbuf, &res);
      else if (SAN[1] >= '1' && SAN[1] <= '8') // rank hint
        err = parse_piece_capture_rank_hint(SAN, game, res.to, piece, mbuf, &res);
      else
        return 1;
    }
    else
    {
      if (!is_valid_square_name(SAN[1], SAN[2]) ||
          game->dynamics.board.types[res.to] != PT_NONE)
        return 1;

      res.from = square_from_name(SAN[1], SAN[2]);
      res.capture = PT_NONE;
    }
    break;
  case 6: // capture, ambiguous
    if (!is_valid_square_name(SAN[1], SAN[2]) || SAN[3] != 'x')
      return 1;

    res.from = square_from_name(SAN[1], SAN[2]);
    break;
  default:
    return 1;
  }

  if (err)
    return err;

  if (move_out)
    *move_out = res;
  return 0;
}

int
parse_SAN(const char *SAN, game_state *game, move_t *move_out, const char **end_ptr)
{
  size_t len;
  move_t res;
  int err;

  for (len = 0; len < strlen(SAN); ++len)
  {
    // TODO: clarify that stops parsing after whitespace
    if (SAN[len] == ' ' || SAN[len] == '\0')
      break;
  }

  // TODO: castle things

  // remove check or mate indicator
  if (SAN[len - 1] == '+' ||
      SAN[len - 1] == '#')
    --len;

  if (len < 2) return 1;

  if (SAN[0] >= 'a' && SAN[0] <= 'h') // pawn move
  {
    err = parse_pawn_move(SAN, len, game, &res);
  }
  else // piece move
  {
    err = parse_piece_move(SAN, len, game, &res);
  }

  if (err)
    return err;
  if (move_out)
    *move_out = res;
  if (end_ptr)
    *end_ptr = &SAN[len];
  return 0;
}

static int
LAN_is_promotion_symbol(char c)
{
  return
    c == 'n' ||
    c == 'b' ||
    c == 'r' ||
    c == 'q';
}

static int
LAN_is_matching_promotion(char c, enum MOVE_TYPE mt)
{
  return
    ((c == 'n' && mt == MT_PROMOTION_KNIGHT) ||
     (c == 'b' && mt == MT_PROMOTION_BISHOP) ||
     (c == 'r' && mt == MT_PROMOTION_ROOK  ) ||
     (c == 'q' && mt == MT_PROMOTION_QUEEN ));
}

static int
move_is_promotion(move_t *m)
{
  return
    m->type == MT_PROMOTION_KNIGHT ||
    m->type == MT_PROMOTION_BISHOP ||
    m->type == MT_PROMOTION_ROOK   ||
    m->type == MT_PROMOTION_QUEEN;
}

int
parse_LAN(const char *LAN, game_state *game, move_t *move_out, const char **end_ptr)
{
  move_t *m;
  move_buffer mbuf;
  square from, to;
  char promotion;
  int fitting_promotion;

  if (strlen(LAN) < 4)
    return 1;

  if (!is_valid_square_name(LAN[0], LAN[1]) ||
      !is_valid_square_name(LAN[2], LAN[3]))
    return 1;

  from = square_from_name(LAN[0], LAN[1]);
  to   = square_from_name(LAN[2], LAN[3]);
  promotion = LAN[4];

  generate_moves(game, &mbuf);
  while ((m = move_buffer_next(&mbuf)) != NULL)
  {
    if (move_is_promotion(m))
      fitting_promotion = LAN_is_promotion_symbol(promotion) && LAN_is_matching_promotion(promotion, m->type);
    else
      fitting_promotion = !LAN_is_promotion_symbol(promotion);

    if (m->from == from &&
        m->to   == to   &&
        fitting_promotion)
      break;
  }

  if (m == NULL)
    return 1;
  if (move_out)
    *move_out = *m;
  if (end_ptr)
    *end_ptr = &LAN[4 + (LAN_is_promotion_symbol(promotion) ? 1 : 0)];
  return 0;
}

#define PV_MAX_LENGTH 8
void
print_pv(game_state *game, trans_table_t *tt)
{
  size_t i;
  move_t m;
  struct tt_entry *entry;
  game_state game_copy = *game;

  for (i = 0; i < PV_MAX_LENGTH; ++i)
  {
    entry = tt_probe(tt, game->statics.zhash);
    if (!entry)
      break;

    m = entry->best;
    print_move(&game_copy.dynamics.board, m);
    printf(" [%d]      ", entry->score);
    move_make(&m, &game_copy);
  }
}
