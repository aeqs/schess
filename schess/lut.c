#include <immintrin.h>
#include <schess/lut.h>
#include <schess/utils.h>

bitboard lut_bishop_mask    [NUM_SQUARES];
bitboard lut_bishop_offset  [NUM_SQUARES];
bitboard lut_bishop_attacks [LUT_BISHOP_SIZE];

bitboard lut_rook_mask      [NUM_SQUARES];
bitboard lut_rook_offset    [NUM_SQUARES];
bitboard lut_rook_attacks   [LUT_ROOK_SIZE];

bitboard lut_king_attacks   [NUM_SQUARES];
bitboard lut_knight_attacks [NUM_SQUARES];

static const bitboard file_attack = 0x0001010101010100;
static const bitboard rank_attack = 0x000000000000007E;
static const bitboard not_a_file = ~0x0101010101010101;
static const bitboard not_h_file = ~0x8080808080808080;
static const bitboard not_1_rank = ~0x00000000000000FF;
static const bitboard not_8_rank = ~0xFF00000000000000;
static const bitboard no_edges = not_a_file & not_h_file & not_1_rank & not_8_rank;

// directional move functions
static inline bitboard no(bitboard b) { return (b << 8); }
static inline bitboard so(bitboard b) { return (b >> 8); }
static inline bitboard ea(bitboard b) { return (b << 1) & not_a_file; }
static inline bitboard we(bitboard b) { return (b >> 1) & not_h_file; }
static inline bitboard noea(bitboard b) { return no(ea(b)); }
static inline bitboard soea(bitboard b) { return so(ea(b)); }
static inline bitboard nowe(bitboard b) { return no(we(b)); }
static inline bitboard sowe(bitboard b) { return so(we(b)); }
static inline bitboard nonoea(bitboard b) { return no(noea(b)); }
static inline bitboard noeaea(bitboard b) { return ea(noea(b)); }
static inline bitboard soeaea(bitboard b) { return ea(soea(b)); }
static inline bitboard sosoea(bitboard b) { return so(soea(b)); }
static inline bitboard nonowe(bitboard b) { return no(nowe(b)); }
static inline bitboard nowewe(bitboard b) { return we(nowe(b)); }
static inline bitboard sowewe(bitboard b) { return we(sowe(b)); }
static inline bitboard sosowe(bitboard b) { return so(sowe(b)); }

/* KNIGHT LOOK UP TABLE */
static void
lut_fill_knight_attacks(void)
{
  square sq;
  bitboard knight;

  for (sq = a1; sq < NUM_SQUARES; ++sq)
  {
    knight = sq2bb(sq);
    lut_knight_attacks[sq]  = nonoea(knight);
    lut_knight_attacks[sq] |= noeaea(knight);
    lut_knight_attacks[sq] |= soeaea(knight);
    lut_knight_attacks[sq] |= sosoea(knight);
    lut_knight_attacks[sq] |= nonowe(knight);
    lut_knight_attacks[sq] |= nowewe(knight);
    lut_knight_attacks[sq] |= sowewe(knight);
    lut_knight_attacks[sq] |= sosowe(knight);
  }
}

/* ROOK LOOK UP TABLE */
static inline bitboard
lut_calc_rook_attacks(bitboard occ, square sq)
{
  bitboard rook, attacks;

  attacks = 0;

#define LUT_CALC_ROOK_ATTACKS_DIR(dir) do \
  { \
    rook = sq2bb(sq); \
    while (rook) \
    { \
      rook = dir(rook); \
      attacks |= rook; \
      rook &= ~occ; \
    } \
  } while (0)

  LUT_CALC_ROOK_ATTACKS_DIR(no);
  LUT_CALC_ROOK_ATTACKS_DIR(so);
  LUT_CALC_ROOK_ATTACKS_DIR(ea);
  LUT_CALC_ROOK_ATTACKS_DIR(we);

#undef LUT_CALC_ROOK_ATTACKS_DIR

  return attacks;
}

static void
lut_fill_rook_attacks(void)
{
  square sq;
  size_t offset;
  unsigned file, rank, num_entries;
  bitboard board;

  offset = 0;

  for (sq = a1; sq < NUM_SQUARES; ++sq)
  {
    lut_rook_offset[sq] = offset;

    // generate mask
    file = sq & BITMASK(3);
    rank = sq >> 3;
    lut_rook_mask[sq]  = file_attack << file;
    lut_rook_mask[sq] |= rank_attack << (rank << 3);
    lut_rook_mask[sq] &= ~sq2bb(sq);

    // GCC
    num_entries = __builtin_popcountll(lut_rook_mask[sq]);

    for (board = 0; board < (1ull << num_entries); ++board)
    {
      // X86
      lut_rook_attacks[offset++] = lut_calc_rook_attacks(_pdep_u64(board, lut_rook_mask[sq]), sq);
    }
  }
}

/* BISHOP LOOK UP TABLE */
static inline bitboard
lut_calc_bishop_attacks(bitboard occ, square sq)
{
  bitboard bishop, attacks;

  attacks = 0;

#define LUT_CALC_BISHOP_ATTACKS_DIR(dir) do \
  { \
    bishop = sq2bb(sq); \
    while (bishop) \
    { \
      bishop = dir(bishop); \
      attacks |= bishop; \
      bishop &= ~occ; \
    } \
  } while (0)

  LUT_CALC_BISHOP_ATTACKS_DIR(noea);
  LUT_CALC_BISHOP_ATTACKS_DIR(soea);
  LUT_CALC_BISHOP_ATTACKS_DIR(nowe);
  LUT_CALC_BISHOP_ATTACKS_DIR(sowe);

#undef LUT_CALC_BISHOP_ATTACKS_DIR

  return attacks;
}

static void
lut_fill_bishop_attacks(void)
{
  square sq;
  size_t offset;
  unsigned num_entries;
  bitboard board;

  offset = 0;

  for (sq = a1; sq < NUM_SQUARES; ++sq)
  {
    lut_bishop_offset[sq] = offset;

    // generate mask
    lut_bishop_mask[sq] = 0;
    for (board = sq2bb(sq); board; board = noea(board), lut_bishop_mask[sq] |= board);
    for (board = sq2bb(sq); board; board = soea(board), lut_bishop_mask[sq] |= board);
    for (board = sq2bb(sq); board; board = nowe(board), lut_bishop_mask[sq] |= board);
    for (board = sq2bb(sq); board; board = sowe(board), lut_bishop_mask[sq] |= board);
    lut_bishop_mask[sq] &= no_edges;

    // GCC
    num_entries = __builtin_popcountll(lut_bishop_mask[sq]);

    for (board = 0; board < (1ull << num_entries); ++board)
    {
      // X86
      lut_bishop_attacks[offset++] = lut_calc_bishop_attacks(_pdep_u64(board, lut_bishop_mask[sq]), sq);
    }
  }
}

/* KING LOOK UP TABLE */
static void
lut_fill_king_attacks(void)
{
  square sq;
  bitboard king;

  for (sq = a1; sq < NUM_SQUARES; ++sq)
  {
    king = sq2bb(sq);
    lut_king_attacks[sq]  = no(king);
    lut_king_attacks[sq] |= so(king);
    lut_king_attacks[sq] |= ea(king);
    lut_king_attacks[sq] |= we(king);
    lut_king_attacks[sq] |= noea(king);
    lut_king_attacks[sq] |= soea(king);
    lut_king_attacks[sq] |= nowe(king);
    lut_king_attacks[sq] |= sowe(king);
  }
}

void
lut_init(void)
{
  lut_fill_knight_attacks();
  lut_fill_bishop_attacks();
  lut_fill_rook_attacks();
  lut_fill_king_attacks();
}
