// TODO: remove
#define _POSIX_C_SOURCE 200809L
#include <fcntl.h>
#include <schess/ntn.h>
#include <schess/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

typedef unsigned char data_t;

struct ntup_net
{
  size_t tuple_count, *tuple_sizes, total_size;
  square *nodes_raw, **nodes;
  data_t *weights_raw, **weights;
};


static size_t
powsz(size_t X, size_t Y)
{
  size_t res, i;

  res = 1;
  for (i = 0; i < Y; ++i)
    res *= X;

  return res;
}

ntup_net *
ntn_create(size_t tuple_count, size_t tuple_sizes[tuple_count], square *nodes[tuple_count])
{
  ntup_net *ntn;
  size_t total_size, nodes_size, weights_size, i, j;

  weights_size = nodes_size = total_size = 0;
  for (i = 0; i < tuple_count; ++i)
  {
    nodes_size += tuple_sizes[i];                                  // .nodes_raw
    weights_size += powsz(PT_COUNT, tuple_sizes[i]);               // .weights_raw
  }
  total_size += tuple_count * sizeof(size_t);                      // .tuple_sizes
  total_size += tuple_count * sizeof(square *);                    // .nodes
  total_size += tuple_count * sizeof(data_t *);                    // .weights
  total_size += nodes_size * sizeof(square);
  total_size += weights_size * sizeof(data_t);
  total_size += sizeof(ntup_net);

  ntn = (ntup_net *) malloc(total_size);
  if (ntn == NULL)
    return NULL;

  ntn->total_size  = total_size;
  ntn->tuple_count = tuple_count;
  ntn->tuple_sizes = (size_t *)  (ntn              + 1);
  ntn->nodes_raw   = (square *)  (ntn->tuple_sizes + tuple_count);
  ntn->nodes       = (square **) (ntn->nodes_raw   + nodes_size);
  ntn->weights_raw = (data_t *)  (ntn->nodes       + tuple_count);
  ntn->weights     = (data_t **) (ntn->weights_raw + weights_size);

  memcpy(ntn->tuple_sizes, tuple_sizes, tuple_count * sizeof(size_t));
  memset(ntn->weights_raw, 0, weights_size * sizeof(data_t));

  nodes_size = 0;
  for (i = 0; i < tuple_count; ++i)
  {
    ntn->nodes[i] = &(ntn->nodes_raw)[nodes_size];
    nodes_size += tuple_sizes[i];

    for (j = 0; j < tuple_sizes[i]; ++j)
      ntn->nodes[i][j] = nodes[i][j];
  }

  weights_size = 0;
  for (i = 0; i < tuple_count; ++i)
  {
    ntn->weights[i] = &(ntn->weights_raw)[weights_size];
    weights_size += powsz(PT_COUNT, tuple_sizes[i]);
  }

  return ntn;
}

void
ntn_destroy(ntup_net *ntn)
{
  free(ntn);
}

static size_t
board_to_idx(board_state *board, square *nodes, size_t tuple_size)
{
  size_t res, i;

  if (tuple_size == 0)
    return 0;

  res = board->types[nodes[0]];
  for (i = 1; i < tuple_size; ++i)
  {
    res *= PT_COUNT;
    res += board->types[nodes[i]];
  }

  return res;
}

void
ntn_update(ntup_net *ntn, board_state *board, int score, float learning_rate)
{
  size_t i, idx;
  data_t diff;

  diff = (data_t) score - ntn_evaluate(ntn, board);

  for (i = 0; i < ntn->tuple_count; ++i)
  {
    idx = board_to_idx(board, ntn->nodes[i], ntn->tuple_sizes[i]);
    ntn->weights[i][idx] += learning_rate * diff;
  }
}

int
ntn_evaluate(ntup_net *ntn, board_state *board)
{
  data_t score;
  size_t i, idx;

  score = 0;
  for (i = 0; i < ntn->tuple_count; ++i)
  {
    idx = board_to_idx(board, ntn->nodes[i], ntn->tuple_sizes[i]);
    score += ntn->weights[i][idx];
  }

  return (int) score;
}

// LINUX
void
ntn_save(ntup_net *ntn, const char *fname)
{
  int fd;
  void *file;

  fd = open(fname, O_CREAT | O_RDWR, 0666);
  if (fd == -1)
    perror("ntn_save():");

  if (ftruncate(fd, ntn->total_size))
    perror("ntn_save():");

  file = mmap(0, ntn->total_size, PROT_WRITE, MAP_SHARED, fd, 0);
  if (file == MAP_FAILED)
    perror("ntn_save():");

  memcpy(file, ntn, ntn->total_size);

  if (munmap(file, ntn->total_size))
    perror("ntn_save():");

  if (close(fd))
    perror("ntn_save():");
}

// LINUX
ntup_net *
ntn_load(const char *fname)
{
  struct stat st;
  size_t total_size;
  int fd;
  ntup_net *ntn, *file;

  if (stat(fname, &st))
    perror("ntn_load():");

  total_size = st.st_size;

  ntn = malloc(total_size);
  if (ntn == NULL)
    return NULL;

  fd = open(fname, O_RDONLY);
  if (fd == -1)
    perror("ntn_load():");

  file = (ntup_net *) mmap(0, total_size, PROT_READ, MAP_PRIVATE, fd, 0);
  if (file == MAP_FAILED)
    perror("ntn_load():");

  memcpy(ntn, file, total_size);

  if (munmap(file, total_size))
    perror("ntn_load():");

  if (close(fd))
    perror("ntn_load():");

  return ntn;
}
