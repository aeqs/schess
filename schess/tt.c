#include <schess/tt.h>
#include <schess/types.h>
#include <stdlib.h>

struct trans_table
{
  size_t bucket_size;
  hash_t mask;
  struct tt_entry *entries;
};

static size_t
bucket_idx(trans_table_t *tt, hash_t zhash)
{
  return (zhash & tt->mask) * tt->bucket_size;
}

/**
 * Retrieves entry for given hash.
 * Returns `NULL` if none have been found.
 **/
struct tt_entry *
tt_probe(trans_table_t *tt, hash_t zhash)
{
  size_t bucket, i;

  bucket = bucket_idx(tt, zhash);

  for (i = 0; i < tt->bucket_size; ++i)
  {
    if (tt->entries[bucket + i].zhash == zhash)
      return &tt->entries[bucket + i];
  }

  return NULL;
}

/**
 * Stores given entry in the table.
 * Uses free space in bucket if available;
 * Replaces entry with lowest depth otherwise.
 **/
void
tt_store(trans_table_t *tt, struct tt_entry entry)
{
  size_t bucket, i, shallowest;
  struct tt_entry *tte;

  bucket = bucket_idx(tt, entry.zhash);
  shallowest = 0;

  for (i = 0; i < tt->bucket_size; ++i)
  {
    tte = &tt->entries[bucket + i];

    if (tte->type == TT_EMPTY)
    {
      *tte = entry;
      return;
    }

    if (tte->zhash == entry.zhash)
    {
      if (tte->depth < entry.depth)
        *tte = entry;

      return;
    }

    if (tte->depth < tt->entries[bucket + shallowest].depth)
      shallowest = i;
  }

  tt->entries[bucket + shallowest] = entry;
}

trans_table_t *
tt_create(size_t capacity, size_t bucket_size)
{
  trans_table_t *tt;

  if (capacity == 0)
    return NULL;

  // set capacity to be next lower power of two
  // TODO: danger; wrong word size
  // TODO: remove 63 const
  // GCC
  capacity = (1ull << (63 - __builtin_clzl(capacity)));

  tt = (trans_table_t *) malloc(sizeof(trans_table_t));
  if (tt == NULL)
    return NULL;

  tt->bucket_size = bucket_size;
  tt->mask        = capacity - 1;
  tt->entries     = (struct tt_entry *) calloc(capacity * bucket_size, sizeof(struct tt_entry));
  if (tt->entries == NULL)
  {
    free(tt);
    return NULL;
  }

  return tt;
}

void
tt_destroy(trans_table_t *tt)
{
  free(tt->entries);
  free(tt);
}
