#include <immintrin.h>
#include <schess/eval.h>
#include <schess/gen.h>
#include <schess/move.h>
#include <schess/tt.h>
#include <schess/utils.h>
#include <string.h>

static inline void
bitboard_set(square sq, bitboard *b) { *b |= sq2bb(sq); }

static inline void
bitboard_unset(square sq, bitboard *b) { *b &= ~sq2bb(sq); }

int move_is_null(move_t m)
{
  return
    m.from      == 0 &&
    m.to        == 0 &&
    m.type      == 0 &&
    m.heuristic == 0;
}

static const bitboard CASTLE_Qkq = 0xFF0000000000000F,
                      CASTLE_Kkq = 0xFF000000000000F0,
                      CASTLE_KQq = 0x0F000000000000FF,
                      CASTLE_KQk = 0xF0000000000000FF,
                      CASTLE_kq  = 0xFF00000000000000,
                      CASTLE_KQ  = 0x00000000000000FF;

static hash_t zobrist_unset_irreversable(irreversible_state meta);
static hash_t zobrist_increment(move_t m, irreversible_state meta, piece_type mover);

int
move_make(move_t *m, game_state *game)
{
  board_state *board = &game->dynamics.board;
  piece_type piece = board->types[m->from],
  capture = board->types[m->to],
  piece_original = piece; // TODO: find better solution for zobrist_increment()

  game->statics.zhash = zobrist_unset_irreversable(game->statics);

  // clear board
  bitboard_unset(m->from, &board->bitboards[piece]);
  bitboard_unset(m->to, &board->bitboards[capture]);
  board->types[m->from] = PT_NONE;

  if (capture != PT_NONE) game->statics.halfmove_clock = 0;

  game->statics.en_passant_potential = EP_NO_POTENTIAL;

  piece_type promo_type, castle_rook;

  switch (m->type)
  {
  case MT_NORMAL:
    if (piece == PT_WP || piece == PT_BP) game->statics.halfmove_clock = 0;
    break;
  case MT_DOUBLE_PAWN:
    // position of en passant potential capture: (from + to) / 2
    game->statics.en_passant_potential = (m->from + m->to) >> 1;
    game->statics.halfmove_clock = 0;
    break;
  case MT_EN_PASSANT:
    if (piece == PT_WP)
    {
      bitboard_unset(m->to - 8, &board->bitboards[PT_BP]);
      board->types[m->to - 8] = PT_NONE;
    }
    else // piece = PT_BP
    {
      bitboard_unset(m->to + 8, &board->bitboards[PT_WP]);
      board->types[m->to + 8] = PT_NONE;
    }
    game->statics.halfmove_clock = 0;
    break;

  case MT_CASTLE_KING:
    castle_rook = piece + (PR_R - PR_K);
    bitboard_unset(m->to + 1, &board->bitboards[castle_rook]);
    bitboard_set(m->from + 1, &board->bitboards[castle_rook]);
    board->types[m->to + 1] = PT_NONE;
    board->types[m->from + 1] = castle_rook;
    break;
  case MT_CASTLE_QUEEN:
    castle_rook = piece + (PR_R - PR_K);
    bitboard_unset(m->to - 2, &board->bitboards[castle_rook]);
    bitboard_set(m->from - 1, &board->bitboards[castle_rook]);
    board->types[m->to - 2] = PT_NONE;
    board->types[m->from - 1] = castle_rook;
    break;

#define MOVE_MAKE_HANDLE_PROMOTION(rel_type) \
    { \
      promo_type = piece + (rel_type - PR_P); \
      game->statics.halfmove_clock = 0; \
      piece = promo_type; \
    }
  case MT_PROMOTION_KNIGHT: MOVE_MAKE_HANDLE_PROMOTION(PR_N); break;
  case MT_PROMOTION_BISHOP: MOVE_MAKE_HANDLE_PROMOTION(PR_B); break;
  case MT_PROMOTION_ROOK: MOVE_MAKE_HANDLE_PROMOTION(PR_R); break;
  case MT_PROMOTION_QUEEN: MOVE_MAKE_HANDLE_PROMOTION(PR_Q); break;
#undef MOVE_MAKE_HANDLE_PROMOTION
  }


  // unset castle rights if rook is taken
  switch (m->to)
  {
  case h1: game->statics.castling_rights &= CASTLE_Qkq; break;
  case a1: game->statics.castling_rights &= CASTLE_Kkq; break;
  case h8: game->statics.castling_rights &= CASTLE_KQq; break;
  case a8: game->statics.castling_rights &= CASTLE_KQk; break;
  default: break;
  }

  switch (m->from)
  {
  case h1: game->statics.castling_rights &= CASTLE_Qkq; break; // white rook kingside moved
  case a1: game->statics.castling_rights &= CASTLE_Kkq; break; // white rook queenside moved
  case h8: game->statics.castling_rights &= CASTLE_KQq; break; // black rook kingside moved
  case a8: game->statics.castling_rights &= CASTLE_KQk; break; // black rook queenside moved
  case e1: game->statics.castling_rights &= CASTLE_kq ; break; // white king moved
  case e8: game->statics.castling_rights &= CASTLE_KQ ; break; // black king moved
  default: break;
  }

  // set board
  bitboard_set(m->to, &board->bitboards[piece]);
  board->types[m->to] = piece;

  game->dynamics.active = OTHER_COLOR(game->dynamics.active);
  game->statics.zhash = zobrist_increment(*m, game->statics, piece_original);

  // TODO: clarify: negamax want positive values
  if (capture == PT_WK) return +oo;//return -oo;
  if (capture == PT_BK) return +oo;
  return 0;
}


void
move_unmake(move_t *m, game_state *game)
{
  board_state *board = &game->dynamics.board;
  piece_type piece = board->types[m->to],
  capture = m->capture;

  bitboard_unset(m->to, &board->bitboards[piece]);
  bitboard_set(m->to, &board->bitboards[capture]);
  board->types[m->to] = capture;

  piece_type prepromo_type, castle_rook;

  switch (m->type)
  {
  case MT_NORMAL:
  case MT_DOUBLE_PAWN:
    break;
  case MT_EN_PASSANT:
    if (piece == PT_WP)
    {
      bitboard_set(m->to - 8, &board->bitboards[PT_BP]);
      board->types[m->to - 8] = PT_BP;
    }
    else // piece = PT_BP
    {
      bitboard_set(m->to + 8, &board->bitboards[PT_WP]);
      board->types[m->to + 8] = PT_WP;
    }
    break;

  case MT_CASTLE_KING:
    castle_rook = piece + (PR_R - PR_K);
    bitboard_unset(m->from + 1, &board->bitboards[castle_rook]);
    bitboard_set(m->to + 1, &board->bitboards[castle_rook]);
    board->types[m->to + 1] = castle_rook;
    board->types[m->from + 1] = PT_NONE;
    break;
  case MT_CASTLE_QUEEN:
    castle_rook = piece + (PR_R - PR_K);
    bitboard_unset(m->from - 1, &board->bitboards[castle_rook]);
    bitboard_set(m->to - 2, &board->bitboards[castle_rook]);
    board->types[m->to - 2] = castle_rook;
    board->types[m->from - 1] = PT_NONE;
    break;

#define MOVE_UNMAKE_HANDLE_PROMOTION(rel_type) \
    { \
      prepromo_type = piece + (PR_P - rel_type); \
      piece = prepromo_type; \
    }
  case MT_PROMOTION_KNIGHT: MOVE_UNMAKE_HANDLE_PROMOTION(PR_N); break;
  case MT_PROMOTION_BISHOP: MOVE_UNMAKE_HANDLE_PROMOTION(PR_B); break;
  case MT_PROMOTION_ROOK: MOVE_UNMAKE_HANDLE_PROMOTION(PR_R); break;
  case MT_PROMOTION_QUEEN: MOVE_UNMAKE_HANDLE_PROMOTION(PR_Q); break;
#undef MOVE_UNMAKE_HANDLE_PROMOTION
  }

  bitboard_set(m->from, &board->bitboards[piece]);
  board->types[m->from] = piece;

  game->dynamics.active = OTHER_COLOR(game->dynamics.active);
}

static bitboard castling_mask = 0x4400000000000044;

static hash_t zobrist_random_pieces[PT_COUNT][NUM_SQUARES],
            zobrist_random_active,
            zobrist_random_castling[16], // 2^4 randoms (each combination)
            zobrist_random_ep_file[8];

static inline hash_t
random_hash(void)
{
  size_t i, num_rand_calls, int_bits;
  hash_t res;

  num_rand_calls = (sizeof(hash_t) + sizeof(int) + 1) / sizeof(int);
  int_bits = sizeof(int) * 8;
  res = 0;

  for (i = 0; i < num_rand_calls; ++i)
  {
    res <<= int_bits;
    res  |= (hash_t) rand();
  }

  return res;
}

void
zobrist_random_init(int seed)
{
  piece_type pt;
  square sq;
  size_t i;

  srand(seed);

  for (pt = 0; pt < PT_COUNT; ++pt)
  {
    for (sq = a1; sq < NUM_SQUARES; ++sq)
    {
      zobrist_random_pieces[pt][sq] = random_hash();
    }
  }

  zobrist_random_active = random_hash();

  for (i = 0; i < (sizeof(zobrist_random_castling) / sizeof(hash_t)); ++i)
    zobrist_random_castling[i] = random_hash();

  for (i = 0; i < (sizeof(zobrist_random_ep_file) / sizeof(hash_t)); ++i)
    zobrist_random_ep_file[i] = random_hash();

  // PT_NONE captures don't change hash value
  memset(zobrist_random_pieces[PT_NONE], 0, NUM_SQUARES * sizeof(hash_t));
}

// unset ep potential, castling rights
hash_t
zobrist_unset_irreversable(irreversible_state meta)
{
  hash_t res;
  unsigned file;

  res  = meta.zhash;
  // X86
  res ^= zobrist_random_castling[_pext_u64(meta.castling_rights, castling_mask)];

  if (meta.en_passant_potential != EP_NO_POTENTIAL)
  {
    file = meta.en_passant_potential & BITMASK(3);
    res ^= zobrist_random_ep_file[file];
  }

  return res;
}

hash_t
zobrist_increment(move_t m, irreversible_state meta, piece_type mover)
{
  hash_t res;
  square sq;

  res  = meta.zhash;

  res ^= zobrist_random_pieces[m.capture][m.to];
  res ^= zobrist_random_pieces[mover][m.from];

  switch (m.type)
  {
  case MT_NORMAL:
    break;
  case MT_DOUBLE_PAWN:
    res ^= zobrist_random_ep_file[(m.from + m.to) >> 1];
    break;
  case MT_EN_PASSANT:
    sq = (m.from & (64 - 8)) | (m.to & BITMASK(3)); // TODO: make cleaner
    res ^= zobrist_random_pieces[OTHER_COLOR(COLOR_OF(mover)) + PR_P][sq];
    break;
  case MT_CASTLE_KING:
    res ^= zobrist_random_pieces[COLOR_OF(mover) + PR_R][m.to   + 1];
    res ^= zobrist_random_pieces[COLOR_OF(mover) + PR_R][m.from + 1];
    break;
  case MT_CASTLE_QUEEN:
    res ^= zobrist_random_pieces[COLOR_OF(mover) + PR_R][m.to   - 2];
    res ^= zobrist_random_pieces[COLOR_OF(mover) + PR_R][m.from - 1];
    break;
  case MT_PROMOTION_KNIGHT:
    mover = COLOR_OF(mover) + PR_N;
    break;
  case MT_PROMOTION_BISHOP:
    mover = COLOR_OF(mover) + PR_B;
    break;
  case MT_PROMOTION_ROOK:
    mover = COLOR_OF(mover) + PR_R;
    break;
  case MT_PROMOTION_QUEEN:
    mover = COLOR_OF(mover) + PR_Q;
    break;
  }

  res ^= zobrist_random_pieces[mover][m.to];

  // X86
  res ^= zobrist_random_castling[_pext_u64(meta.castling_rights, castling_mask)];
  res ^= zobrist_random_active;

  return res;
}


int
post_move_king_safety(move_t *m, game_state *game)
{
  int res;
  color active = game->dynamics.active;

  move_make(m, game);
  res = king_safety(&game->dynamics.board, active);
  move_unmake(m, game);

  return res;
}
