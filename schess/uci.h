#ifndef SCHESS_UCI_H
#define SCHESS_UCI_H

#include <schess/types.h>

int uci_init(void);
int uci_handle_command(const char *cmd);

#endif // SCHESS_UCI_H
