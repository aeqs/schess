#ifndef TEST_EPD_H
#define TEST_EPD_H

// default test movetime is 2 minutes
#define EPD_DEFAULT_MOVETIME 120000

int epd_test(const char *epd, unsigned long msecs);

#endif // TEST_EPD_H
