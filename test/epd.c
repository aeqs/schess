#include <schess/search.h>
#include <schess/time.h>
#include <schess/tt.h>
#include <schess/types.h>
#include <schess/utils.h>
#include <string.h>
#include <test/base.h>

int
epd_test(const char *epd, unsigned long msecs)
{
  game_state game;
  move_t best, parsed;
  struct search_state sstate;
  struct search_limits limits;

  const char *epd_ptr;
  int err;

  err = parse_FEN(epd, &game, &epd_ptr);
  if (err)
    return err;

  if (!str_skip_blanks(&epd_ptr))
    return 1;

  limits = search_limits_infinite;
  limits.time = msecs;

  (sstate.tt = tt_create(1 << 15, 4))   ASSERT_NOT(NULL, 1, "epd_test(): tt_create()");
  (sstate.qstt = tt_create(1 << 15, 4)) ASSERT_NOT(NULL, 1, "epd_test(): tt_create()");
  (sstate.sc = sc_create())             ASSERT_NOT(NULL, 1, "epd_test(): sc_create()");
  best = search_root(&game, sstate, limits).move;
  tt_destroy(sstate.tt);
  tt_destroy(sstate.qstt);
  sc_destroy(sstate.sc);

  while (*epd_ptr)
  {
    while (*epd_ptr == ' ')
      ++epd_ptr;

    if (str_match(&epd_ptr, "bm"))
    {
      while (str_skip_blanks(&epd_ptr))
      {
        err = parse_SAN(epd_ptr, &game, &parsed, &epd_ptr);
        if (err)
          return err;

        if (!memcmp(&best, &parsed, sizeof(move_t)))
          return 0; // best move found in bm
      }
    }
    else if (str_match(&epd_ptr, "am"))
    {
      while (str_skip_blanks(&epd_ptr))
      {
        err = parse_SAN(epd_ptr, &game, &parsed, &epd_ptr);
        if (err)
          return err;

        if (!memcmp(&best, &parsed, sizeof(move_t)))
          return 1; // best move found in am
      }
    }

    while (*epd_ptr != '\0' && *epd_ptr != ';')
      ++epd_ptr;

    if (*epd_ptr++ != ';')
      return 1;
  }

  return 1;
}
